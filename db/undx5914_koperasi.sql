-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2021 at 08:00 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_dimension`
--

CREATE TABLE `d_dimension` (
  `id` int(11) NOT NULL,
  `kode_pos_id` varchar(5) CHARACTER SET utf8 NOT NULL,
  `mantri_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_dimension`
--

INSERT INTO `d_dimension` (`id`, `kode_pos_id`, `mantri_id`, `active`, `created_date`, `updated_date`) VALUES
(1, 'P1', 1, 1, '2021-12-26 15:10:09', '2021-12-26 15:10:09'),
(2, 'P1', 2, 1, '2021-12-26 15:10:09', '2021-12-26 15:10:09'),
(4, 'P1', 3, 1, '2021-12-26 15:10:25', '2021-12-26 15:10:25'),
(5, 'P1', 4, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(6, 'P1', 5, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(7, 'P1', 6, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(8, 'P1', 7, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(9, 'P1', 8, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(10, 'P1', 9, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(11, 'P1', 10, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(12, 'P1', 11, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(13, 'P1', 12, 1, '2021-12-26 15:33:36', '2021-12-26 15:33:36'),
(14, 'P2', 1, 1, '2021-12-26 15:34:13', '2021-12-26 15:34:13'),
(15, 'P2', 2, 1, '2021-12-26 15:34:13', '2021-12-26 15:34:13'),
(16, 'P3', 12, 1, '2021-12-26 15:34:13', '2021-12-26 15:34:13');

-- --------------------------------------------------------

--
-- Table structure for table `d_menu_groups`
--

CREATE TABLE `d_menu_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `fk_id_group` mediumint(8) NOT NULL,
  `fk_id_menu` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `d_menu_groups`
--

INSERT INTO `d_menu_groups` (`id`, `fk_id_group`, `fk_id_menu`) VALUES
(1, 1, 1),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 3, 8),
(17, 3, 12),
(18, 3, 14),
(19, 1, 16),
(20, 1, 17);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Super Admin'),
(2, 'manager', 'Manager'),
(3, 'administrator', 'Admin'),
(4, 'k_pos', 'Kepala Pos'),
(5, 'mantri', 'Mantri');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `m_coa`
--

CREATE TABLE `m_coa` (
  `kode_coa` int(12) NOT NULL,
  `kode_coa_1` int(11) DEFAULT NULL,
  `kode_coa_2` int(11) DEFAULT NULL,
  `kode_coa_3` int(11) DEFAULT NULL,
  `kelompok` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `tipe` varchar(50) NOT NULL,
  `kas_setara_kas` int(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_coa`
--

INSERT INTO `m_coa` (`kode_coa`, `kode_coa_1`, `kode_coa_2`, `kode_coa_3`, `kelompok`, `description`, `tipe`, `kas_setara_kas`, `active`, `created_date`, `updated_date`) VALUES
(100000, NULL, NULL, NULL, 'KELOMPOK', 'AKTIVA', 'GRAND TOTAL', 0, 1, '2021-12-19 21:50:23', '2021-12-19 21:50:23'),
(110000, 100000, NULL, NULL, 'NERACA', 'AKTIVA LANCAR', 'SUBTOTAL', 0, 1, '2021-12-19 21:51:46', '2021-12-19 21:51:46'),
(111000, 100000, 110000, NULL, 'NERACA', 'KAS SETARA KAS', 'TOTAL', 0, 1, '2021-12-19 21:52:50', '2021-12-19 21:52:50'),
(111010, 100000, 110000, 111000, 'NERACA', 'KAS KECIL', 'TRANSAKSI', 1, 1, '2021-12-19 21:53:41', '2021-12-19 21:58:39'),
(111020, 100000, 110000, 111000, 'NERACA', 'BANK - BNI', 'TRANSAKSI', 1, 1, '2021-12-19 21:58:32', '2021-12-19 21:58:42'),
(111030, 100000, 110000, 111000, 'NERACA', 'BANK - BCA', 'TRANSAKSI', 1, 1, '2021-12-19 21:58:32', '2021-12-19 21:58:44');

-- --------------------------------------------------------

--
-- Table structure for table `m_kode_pos`
--

CREATE TABLE `m_kode_pos` (
  `kode_pos` varchar(5) NOT NULL,
  `number` int(11) NOT NULL,
  `nm_pos` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_kode_pos`
--

INSERT INTO `m_kode_pos` (`kode_pos`, `number`, `nm_pos`, `active`, `created_date`, `updated_date`) VALUES
('P1', 1, 'Surabaya', 1, '2021-10-14 09:03:02', '2021-12-26 17:00:19'),
('P2', 2, 'Trosobo', 1, '2021-10-14 09:04:59', '2021-12-26 17:00:21'),
('P3', 3, 'Krian', 1, '2021-12-24 22:13:16', '2021-12-26 17:00:23'),
('P4', 4, 'Sukodono', 1, '2021-12-24 22:13:16', '2021-12-26 17:00:24'),
('P5', 5, 'Sidoarjo', 1, '2021-12-24 22:13:16', '2021-12-26 17:00:25'),
('P6', 6, 'Malang', 1, '2021-12-24 22:13:16', '2021-12-26 17:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `m_mantri`
--

CREATE TABLE `m_mantri` (
  `id` int(11) NOT NULL,
  `kode` varchar(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_mantri`
--

INSERT INTO `m_mantri` (`id`, `kode`, `nama`, `active`, `created_date`, `updated_date`) VALUES
(1, '1', 'Mantri 1', 1, '2021-10-11 23:30:55', '2021-10-11 23:30:55'),
(2, '2', 'Mantri 2', 1, '2021-10-11 23:33:57', '2021-10-11 23:33:57'),
(3, '3', 'Mantri 3', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(4, '4', 'Mantri 4', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(5, '5', 'Mantri 5', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(6, '6', 'Mantri 6', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(7, '7', 'Mantri 7', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(8, '8', 'Mantri 8', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(9, '9', 'Mantri 9', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(10, '10', 'Mantri 10', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(11, '11', 'Mantri 11', 1, '2021-12-26 14:48:34', '2021-12-26 14:48:34'),
(12, 'pusat', 'Pusat', 1, '2021-12-26 14:49:22', '2021-12-26 14:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `m_menus`
--

CREATE TABLE `m_menus` (
  `menu_id` int(11) NOT NULL,
  `menu_nm` varchar(100) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `orders` decimal(10,2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_menus`
--

INSERT INTO `m_menus` (`menu_id`, `menu_nm`, `icon`, `color`, `url`, `orders`, `parent_id`, `active`, `keterangan`, `created_date`, `updated_date`) VALUES
(1, 'Dashboard', 'ni-shop text-primary', 'text-primary', 'dashboard', '1.00', NULL, 1, NULL, '2020-12-28 09:36:17', '2021-10-11 23:58:04'),
(2, 'Data Nasabah', 'ni-circle-08', 'text-orange', 'data_nasabah', '2.00', NULL, 0, NULL, '2021-10-08 01:21:40', '2021-10-25 09:45:01'),
(3, 'Kas Kecil', 'ni-books', 'text-yellow', 'kas_kecil', '3.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:07'),
(4, 'Storting', 'ni-send', 'text-default', 'storting', '4.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:25'),
(5, 'Droping', 'ni-curved-next', 'text-info', 'droping', '5.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:27'),
(6, 'Data Analisa', 'ni-sound-wave', 'text-pink', 'data_analisa', '6.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:34'),
(7, 'Laporan Keuangan', 'ni-money-coins', 'text-green', 'laporan_keuangan', '7.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:42'),
(8, 'Data Master', 'ni-box-2', NULL, '', '8.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(9, 'Perhitungan Bunga', NULL, NULL, 'perhitungan_bunga', '8.20', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(10, 'Perhitungan Asset Tetap', NULL, NULL, 'perhitungan_asset_tetap', '8.30', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(11, 'Data Master Leasing', NULL, NULL, 'data_leasing', '8.40', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(12, 'Data Nasabah', NULL, NULL, 'data_nasabah', '8.10', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(13, 'Data Master Vendor', NULL, NULL, 'data_vendor', '8.50', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(14, 'Data Master Karyawan', NULL, NULL, 'data_karyawan', '8.60', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-30 01:31:51'),
(15, 'Trial Balance', 'ni-money-coins', NULL, 'trial_balance', '9.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-30 01:33:22'),
(16, 'Data Master COA', NULL, NULL, 'data_coa', '8.11', 8, 1, NULL, '2021-11-07 17:34:31', '2021-11-07 17:34:31'),
(17, 'Data Master Dimension', NULL, NULL, 'data_master_dimension', '8.70', 8, 1, NULL, '2021-12-24 21:55:37', '2021-12-24 21:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `m_nasabah`
--

CREATE TABLE `m_nasabah` (
  `id` int(11) NOT NULL,
  `nama_nasabah` varchar(255) NOT NULL,
  `kode_nasabah` varchar(100) DEFAULT NULL,
  `tipe_nasabah` varchar(5) NOT NULL DEFAULT 'N',
  `kode_pos_id` varchar(5) DEFAULT NULL,
  `mantri_id` int(11) NOT NULL,
  `running_number` int(25) NOT NULL,
  `kayawan_id` int(11) NOT NULL,
  `ktp` varchar(100) NOT NULL,
  `kk` varchar(255) NOT NULL,
  `alamat_ktp` varchar(255) NOT NULL,
  `alamat_tinggal` varchar(255) NOT NULL,
  `AR` int(11) DEFAULT NULL,
  `NT` int(11) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sts_pending` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_nasabah`
--

INSERT INTO `m_nasabah` (`id`, `nama_nasabah`, `kode_nasabah`, `tipe_nasabah`, `kode_pos_id`, `mantri_id`, `running_number`, `kayawan_id`, `ktp`, `kk`, `alamat_ktp`, `alamat_tinggal`, `AR`, `NT`, `status`, `active`, `sts_pending`, `created_date`, `updated_date`) VALUES
(1, 'NASABAH 11', 'P1.01-0001', 'N', '1', 1, 1, 2, '3121231', '124123', '123123', '', NULL, NULL, '', 1, 1, '2021-10-16 09:55:07', '2021-11-03 05:51:57'),
(3, 'NASABAH 2', 'P1.01-0002', 'N', '1', 1, 2, 0, '2222', '2222', 'jl.2            ', 'jl.2            ', NULL, NULL, '', 0, 0, '2021-10-17 17:48:02', '2021-10-21 21:13:02'),
(4, 'NASABAH 3', 'P1.01-0003', 'N', '1', 2, 3, 0, '33333333', '2147483647', 'jl.3            ', 'jl.3', NULL, NULL, '', 0, 0, '2021-10-17 17:52:01', '2021-10-21 21:26:36'),
(5, 'NASABAH 4', 'P1.01-0004', 'N', '1', 2, 4, 0, '444444444', '2147483647', 'jl.4            ', 'jl.4', NULL, NULL, '', 1, 0, '2021-10-17 17:53:29', '2021-10-21 21:45:31'),
(6, 'Nasabah 5', 'P1.01-0005', 'N', '1', 2, 5, 0, '2147483647', '2147483647', 'asdkm            ', 'askdm            ', NULL, NULL, '', 1, 0, '2021-10-17 18:08:21', '2021-10-21 21:45:34'),
(9, 'ASYIQ', 'P1.01-0006', 'N', '1', 2, 6, 0, '210', '3161', 'ktp            ', 'ktp            ', NULL, NULL, '', 1, 0, '2021-10-19 17:16:13', '2021-10-21 21:45:37'),
(14, 'asyi', 'P1.01-0008', 'N', '1', 1, 8, 0, '999', '78787', 'asyiq            ', 'asdads            ', NULL, NULL, '', 1, 0, '2021-10-19 17:55:54', '2021-10-21 21:45:43'),
(15, 'vvv', 'P1.01-0009', 'N', '1', 2, 9, 0, '3131', '12312', 'adsads', 'asd', NULL, NULL, '', 1, 0, '2021-10-19 17:56:30', '2021-10-21 21:45:54'),
(24, 'uuu', 'P1.01-0010', 'N', '1', 1, 10, 0, '67676', '7676', '            fgh', 'mojokerto', NULL, NULL, '', 1, 0, '2021-10-19 18:01:57', '2021-10-21 21:45:57'),
(25, 'wer', NULL, 'N', '1', 1, 11, 0, '567', '567', '567', '            577', NULL, NULL, '', 1, 0, '2021-10-19 18:03:58', '2021-10-21 21:13:02'),
(29, 'JJ', NULL, 'N', '1', 1, 13, 0, '3524232903220001', '3524232903220001', 'ww', 'ww', NULL, NULL, '', 1, 0, '2021-10-29 23:15:17', '2021-10-29 23:15:17'),
(30, 'ANGGAN ', 'P1.01-0011', 'N', '1', 1, 14, 0, '', '', '            ', '            ', NULL, NULL, '', 1, 0, '2021-10-30 15:00:36', '2021-12-12 14:07:57'),
(31, 'ANGGAN', 'P1.01-0012', 'N', '1', 1, 15, 0, '1124234534', '4563456', '            FGHJ45', '            4356FGNFG', NULL, NULL, '', 1, 0, '2021-12-12 14:06:44', '2021-12-12 14:07:54');

-- --------------------------------------------------------

--
-- Table structure for table `t_bunga`
--

CREATE TABLE `t_bunga` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `kas_kecil` double NOT NULL,
  `jasa` double NOT NULL,
  `bunga` double NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_droping`
--

CREATE TABLE `t_droping` (
  `id` int(11) NOT NULL,
  `kode_nasabah` varchar(255) NOT NULL,
  `tgl` date NOT NULL,
  `no_trans` varchar(255) NOT NULL,
  `nominal` int(255) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` int(11) NOT NULL DEFAULT 1,
  `status` varchar(20) NOT NULL DEFAULT 'Pending',
  `running_number` int(11) NOT NULL,
  `kode_bulan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_droping`
--

INSERT INTO `t_droping` (`id`, `kode_nasabah`, `tgl`, `no_trans`, `nominal`, `update_date`, `created_date`, `active`, `status`, `running_number`, `kode_bulan`) VALUES
(8, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 1),
(9, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 2),
(10, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 3),
(11, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 4),
(12, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 5),
(13, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 6),
(14, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 7),
(15, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 8),
(16, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 9),
(17, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 10),
(18, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 11),
(19, '', '0000-00-00', '', 0, '2021-10-29 17:01:15', '2021-10-29 17:01:15', 0, 'Pending', 0, 12),
(20, 'P1.01-0054', '2021-10-30', 'DRP.10.00001', 50000, '2021-10-29 17:02:00', '2021-10-29 17:02:00', 1, 'Pending', 1, 10),
(21, 'P1.01-0011', '2021-10-29', 'DRP.10.00002', 100000, '2021-11-02 22:47:02', '2021-10-29 17:02:10', 1, 'Approve', 2, 10),
(22, 'P1.01-0266', '2021-10-30', 'DRP.10.00003', 15000, '2021-10-29 20:13:28', '2021-10-29 20:13:28', 1, 'Pending', 3, 10),
(23, '', '2021-12-12', 'DRP.12.00001', 400000, '2021-12-12 07:05:51', '2021-12-12 07:05:51', 1, 'Pending', 1, 12),
(24, '', '2021-12-12', 'DRP.12.00002', 500000, '2021-12-12 07:07:27', '2021-12-12 07:07:27', 1, 'Pending', 2, 12);

-- --------------------------------------------------------

--
-- Table structure for table `t_storting`
--

CREATE TABLE `t_storting` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_trans` varchar(255) NOT NULL,
  `kode_nasabah` varchar(255) NOT NULL,
  `nominal` int(11) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` varchar(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 1,
  `running_number` int(11) NOT NULL,
  `kode_bulan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_storting`
--

INSERT INTO `t_storting` (`id`, `tgl`, `no_trans`, `kode_nasabah`, `nominal`, `update_date`, `create_date`, `status`, `active`, `running_number`, `kode_bulan`) VALUES
(12, '0000-00-00', '', '', 0, '2021-10-29 16:53:26', '2021-10-29 16:53:10', '', 0, 0, 1),
(13, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 2),
(14, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 3),
(15, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 4),
(16, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 5),
(17, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 6),
(18, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 7),
(19, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 8),
(20, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 9),
(21, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 10),
(22, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 11),
(23, '0000-00-00', '', '', 0, '2021-10-29 16:54:10', '2021-10-29 16:53:10', '', 0, 0, 12),
(27, '2021-10-29', 'STR.10.00001', 'P1.01-0054', 50000, '2021-10-29 16:57:34', '2021-10-29 16:57:34', '', 1, 1, 10),
(28, '2021-10-29', 'STR.10.00002', 'P1.01-0011', 50000, '2021-10-29 16:58:28', '2021-10-29 16:58:28', '', 1, 2, 10),
(29, '2021-10-30', 'STR.10.00003', 'P1.01-0189', 50000, '2021-10-29 20:13:51', '2021-10-29 20:13:51', '', 1, 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `no_karyawan` varchar(255) DEFAULT NULL,
  `tipe_karyawan` varchar(1) DEFAULT 'K',
  `kode_pos` int(11) DEFAULT NULL,
  `running_number` int(11) DEFAULT NULL,
  `mantri_id` int(11) DEFAULT NULL,
  `nama_karyawan` varchar(100) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `kk` varchar(255) NOT NULL,
  `alamat_ktp` varchar(255) NOT NULL,
  `alamat_tinggal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `no_karyawan`, `tipe_karyawan`, `kode_pos`, `running_number`, `mantri_id`, `nama_karyawan`, `nik`, `kk`, `alamat_ktp`, `alamat_tinggal`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1640522193, 1, NULL, NULL, NULL, NULL, 'K.01.P1-00000', 'K', 1, 0, 1, 'Administrator', '111111111', '111111112', 'Jl.X', 'Jl.X1'),
(2, '::1', 'manager', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, '', NULL, NULL, NULL, NULL, 1634197586, 1635415870, 1, NULL, NULL, NULL, NULL, 'K.01.P1-00000', 'K', 1, 0, 1, 'Manager', '222222', '22222', 'jl.k1', 'jl.k1'),
(3, '::1', 'admin', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, '', NULL, NULL, NULL, NULL, 1634197746, 1635933476, 1, NULL, NULL, NULL, NULL, 'K.02.P1-00000', 'K', 1, 0, 2, 'Admin', '3333333', '333333', 'jl.k2', 'jl.k2'),
(14, '::1', 'kepalapos1', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, '', NULL, NULL, NULL, NULL, 1635136271, 1635136921, 1, NULL, NULL, NULL, NULL, 'K.01.P1-00000', 'K', 1, 0, 1, 'Kepala Pos1', '12123', '123123', 'ads', 'ads'),
(15, '::1', 'mantri', '$2y$08$vaGjkglw0LhWUwhIhchne.ZdBwpPv.WMvWo97GyLn/zB/fNlYURKi', NULL, '', NULL, NULL, NULL, NULL, 1635891999, NULL, 1, NULL, NULL, NULL, NULL, 'K.01.P1-00001', 'K', 1, 1, 1, 'Mantri', '11223344', '1122334455', 'Mantri1', 'Mantri1');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(3, 2, 2),
(4, 3, 3),
(5, 14, 4),
(6, 15, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_dimension`
--
ALTER TABLE `d_dimension`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`kode_pos_id`,`mantri_id`),
  ADD KEY `dimension_mantri` (`mantri_id`);

--
-- Indexes for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menu` (`fk_id_menu`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_coa`
--
ALTER TABLE `m_coa`
  ADD PRIMARY KEY (`kode_coa`);

--
-- Indexes for table `m_kode_pos`
--
ALTER TABLE `m_kode_pos`
  ADD PRIMARY KEY (`kode_pos`),
  ADD UNIQUE KEY `kode_pos` (`kode_pos`);

--
-- Indexes for table `m_mantri`
--
ALTER TABLE `m_mantri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_menus`
--
ALTER TABLE `m_menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `m_nasabah`
--
ALTER TABLE `m_nasabah`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`ktp`),
  ADD UNIQUE KEY `running_number` (`running_number`),
  ADD KEY `fk_nasabah_mantri` (`mantri_id`);

--
-- Indexes for table `t_droping`
--
ALTER TABLE `t_droping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_storting`
--
ALTER TABLE `t_storting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_mantri` (`mantri_id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_dimension`
--
ALTER TABLE `d_dimension`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `m_mantri`
--
ALTER TABLE `m_mantri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `m_menus`
--
ALTER TABLE `m_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `m_nasabah`
--
ALTER TABLE `m_nasabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `t_droping`
--
ALTER TABLE `t_droping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `t_storting`
--
ALTER TABLE `t_storting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_dimension`
--
ALTER TABLE `d_dimension`
  ADD CONSTRAINT `dimension_kode_pos` FOREIGN KEY (`kode_pos_id`) REFERENCES `m_kode_pos` (`kode_pos`),
  ADD CONSTRAINT `dimension_mantri` FOREIGN KEY (`mantri_id`) REFERENCES `m_mantri` (`id`);

--
-- Constraints for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD CONSTRAINT `fk_menu` FOREIGN KEY (`fk_id_menu`) REFERENCES `m_menus` (`menu_id`);

--
-- Constraints for table `m_nasabah`
--
ALTER TABLE `m_nasabah`
  ADD CONSTRAINT `fk_nasabah_mantri` FOREIGN KEY (`mantri_id`) REFERENCES `m_mantri` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_mantri` FOREIGN KEY (`mantri_id`) REFERENCES `m_mantri` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
