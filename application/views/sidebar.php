<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
	<div class="scrollbar-inner">
		<!-- Brand -->
		<div class="sidenav-header  align-items-center">
			<a class="navbar-brand" href="javascript:void(0)">
				<img src="assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
			</a>
		</div>
		<div class="navbar-inner">
			<!-- Collapse -->
			<div class="collapse navbar-collapse" id="sidenav-collapse-main">
				<!-- Nav items -->
				<ul class="navbar-nav">
					<?php foreach ($_SESSION['session_header_koperasi'] as $key => $menu) {
						if ($menu['id'] != $parent_active) {
							$expanded = 'false';
							$show = '';
						} else {
							$expanded = 'true';
							$show = 'show';
						}
					?>

						<li class="nav-item <?php echo $menu['id'] == $parent_active ? "active " : "" ?>">
							<a class="nav-link <?php echo $menu['id'] == $parent_active ? "active" : "" ?>" href="<?php echo $menu['url'] != null ? base_url() . $menu['url'] : '#navbar-' . $key ?>" <?php echo $menu['url'] != null ? '' : 'data-toggle="collapse" role="button" aria-expanded="'.$expanded.'" aria-controls="navbar-' . $key . '"'  ?>>
								<i class="ni <?php echo $menu['icon'] ?> <?php echo $menu['color'] != null ? $menu['color'] : "text-primary" ?>"></i>
								<span class="nav-link-text"><?php echo $menu['name'] ?></span>
							</a>
							<?php if (array_key_exists('child', $menu)) { ?>
								<div class="collapse <?php echo $show?>" id="navbar-<?php echo $key ?>">
									<ul class="nav nav-sm flex-column">
										<?php foreach ($menu['child'] as $child) { ?>
											<li class="nav-item">
												<a href="<?php echo base_url() . $child['url'] ?>" class="nav-link <?php echo $child['id'] == $child_active ? "active" : "" ?>">
													<span class="sidenav-mini-icon"> <i class="ni <?php echo $child['icon'] ?>"></i> </span>
													<span class="sidenav-normal">&nbsp<?php echo $child['name'] ?></span>
												</a>
											</li>
										<?php } ?>
									</ul>
								</div>
							<?php } ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</nav>
<div class="main-content" id="panel">