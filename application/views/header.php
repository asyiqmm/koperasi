<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>YSMSI - Yapusa Sejahtera Mandiri System Informasi</title>
    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <!-- Datatable -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/argon.css?v=1.2.0" type="text/css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/select2/dist/css/select2.min.css">
    <!-- FullCalendar -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/sweetalert2/dist/sweetalert2.min.css">
    <!-- Quill -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/quill/dist/quill.core.css" type="text/css">
    <!-- Notif -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/animate.css/animate.min.css">
    <!-- Sweet alerts -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/sweetalert2/dist/sweetalert2.min.css">
    <script src="<?php echo base_url() ?>assets/vendor/jquery/dist/jquery.min.js"></script>
    
    <style>
        .select2-container--bootstrap4 .select2-selection--single {
            height: calc(2.25rem + 2px) !important;
        }
    </style>
</head>

<body class="">