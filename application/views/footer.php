</div>
<!-- Argon Scripts -->

<!-- Optional JS -->
<script src="<?php echo base_url() ?>assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->
<script src="<?php echo base_url() ?>assets/js/argon.js?v=1.2.0"></script>
<!-- Datatable -->
<script src="<?php echo base_url() ?>assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
<!-- Date picker -->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Datetime picker -->
<script src="<?php echo base_url() ?>assets/vendor/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-datetimepicker.js"></script>

<!-- Dropzone -->
<script src="<?php echo base_url() ?>assets/vendor/dropzone/dist/min/dropzone.min.js"></script>
<!-- FullCalendar -->
<script src="<?php echo base_url() ?>assets/vendor/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
<!-- Jvector Map -->
<script src="<?php echo base_url() ?>assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
<!-- <script src="<?php echo base_url() ?>assets/js/vendor/jvectormap/jquery-jvectormap-world-mill.js"></script> -->
<!-- List JS -->
<script src="<?php echo base_url() ?>assets/vendor/list.js/dist/list.min.js"></script>
<!-- Quill -->
<script src="<?php echo base_url() ?>assets/vendor/quill/dist/quill.min.js"></script>
<!-- Notif -->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
<!-- Slider -->
<script src="<?php echo base_url() ?>assets/vendor/nouislider/distribute/nouislider.min.js"></script>
<!-- Sweet alerts -->
<script src="<?php echo base_url() ?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
<!-- Tags -->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<!-- Core -->

<script src="<?php echo base_url() ?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/js-cookie/js.cookie.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/vendor/select2/dist/js/select2.min.js"></script>
</body>

</html>