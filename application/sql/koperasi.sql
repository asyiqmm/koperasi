-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2021 at 03:33 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_menu_groups`
--

CREATE TABLE `d_menu_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `fk_id_group` mediumint(8) NOT NULL,
  `fk_id_menu` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `d_menu_groups`
--

INSERT INTO `d_menu_groups` (`id`, `fk_id_group`, `fk_id_menu`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `m_mantri`
--

CREATE TABLE `m_mantri` (
  `id` int(11) NOT NULL,
  `kode` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_mantri`
--

INSERT INTO `m_mantri` (`id`, `kode`, `nama`, `active`, `created_date`, `updated_date`) VALUES
(1, 1, 'Mantri 1', 1, '2021-10-11 23:30:55', '2021-10-11 23:30:55'),
(2, 2, 'Mantri 2', 1, '2021-10-11 23:33:57', '2021-10-11 23:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `m_menus`
--

CREATE TABLE `m_menus` (
  `menu_id` int(11) NOT NULL,
  `menu_nm` varchar(100) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `orders` decimal(10,2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_menus`
--

INSERT INTO `m_menus` (`menu_id`, `menu_nm`, `icon`, `color`, `url`, `orders`, `parent_id`, `active`, `keterangan`, `created_date`, `updated_date`) VALUES
(1, 'Dashboard', 'ni-shop text-primary', 'text-primary', 'dashboard', '1.00', NULL, 1, NULL, '2020-12-28 09:36:17', '2021-10-11 23:58:04'),
(2, 'Data Nasabah', 'ni-circle-08', 'text-orange', 'data_nasabah', '2.00', NULL, 1, NULL, '2021-10-08 01:21:40', '2021-10-12 00:03:48'),
(3, 'Kas Kecil', 'ni-books', 'text-yellow', 'kas_kecil', '3.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:07'),
(4, 'Storting', 'ni-send', 'text-default', 'storting', '4.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:25'),
(5, 'Droping', 'ni-curved-next', 'text-info', 'droping', '5.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:27'),
(6, 'Data Analisa', 'ni-sound-wave', 'text-pink', 'data_analisa', '6.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:34'),
(7, 'Laporan Keuangan', 'ni-money-coins', 'text-green', 'laporan_keuangan', '7.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:42'),
(8, 'Data Master', 'ni-box-2', NULL, '', '8.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(9, 'Perhitungan Bunga', NULL, NULL, 'perhitungan_bunga', '8.20', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(10, 'Perhitungan Asset Tetap', NULL, NULL, 'perhitungan_asset_tetap', '8.30', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(11, 'Data Master Leasing', NULL, NULL, 'data_leasing', '8.40', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(12, 'Data Nasabah', NULL, NULL, 'data_nasabah', '8.10', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(13, 'Data Master Vendor', NULL, NULL, 'data_vendor', '8.50', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(14, 'Data Master Karyawan', NULL, NULL, 'data_karyawan', '8.60', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `m_r_mantri_karyawan`
--

CREATE TABLE `m_r_mantri_karyawan` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `mantri_id` mediumint(8) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_r_mantri_karyawan`
--

INSERT INTO `m_r_mantri_karyawan` (`id`, `user_id`, `mantri_id`, `active`, `created_date`, `updated_date`) VALUES
(1, 1, 1, 1, '2021-10-11 23:53:58', '2021-10-11 23:53:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `tipe_karyawan` varchar(1) DEFAULT 'K',
  `kode_pos` varchar(5) DEFAULT NULL,
  `running_number` int(11) DEFAULT NULL,
  `nama_karyawan` varchar(100) NOT NULL,
  `nik` int(50) NOT NULL,
  `kk` int(50) NOT NULL,
  `alamat_ktp` varchar(255) NOT NULL,
  `alamat_tinggal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `tipe_karyawan`, `kode_pos`, `running_number`, `nama_karyawan`, `nik`, `kk`, `alamat_ktp`, `alamat_tinggal`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1634042101, 1, 'Admin', 'istrator', 'ADMIN', '0', 'K', '1', NULL, 'Administrator', 111111111, 111111112, 'Jl.X', 'Jl.X');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menu` (`fk_id_menu`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_mantri`
--
ALTER TABLE `m_mantri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_menus`
--
ALTER TABLE `m_menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `m_r_mantri_karyawan`
--
ALTER TABLE `m_r_mantri_karyawan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_mantri_karyawan` (`user_id`,`mantri_id`) USING BTREE,
  ADD KEY `fk_mantri_idx` (`user_id`) USING BTREE,
  ADD KEY `fk_m_r_karyawan_idx` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_mantri`
--
ALTER TABLE `m_mantri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_menus`
--
ALTER TABLE `m_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `m_r_mantri_karyawan`
--
ALTER TABLE `m_r_mantri_karyawan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD CONSTRAINT `fk_menu` FOREIGN KEY (`fk_id_menu`) REFERENCES `m_menus` (`menu_id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
