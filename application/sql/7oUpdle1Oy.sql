-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: remotemysql.com
-- Generation Time: Oct 18, 2021 at 01:49 PM
-- Server version: 8.0.13-4
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `7oUpdle1Oy`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_menu_groups`
--

CREATE TABLE `d_menu_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `fk_id_group` mediumint(8) NOT NULL,
  `fk_id_menu` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `d_menu_groups`
--

INSERT INTO `d_menu_groups` (`id`, `fk_id_group`, `fk_id_menu`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `m_kode_pos`
--

CREATE TABLE `m_kode_pos` (
  `id` int(11) NOT NULL,
  `nm_pos` varchar(5) NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_kode_pos`
--

INSERT INTO `m_kode_pos` (`id`, `nm_pos`, `kode_pos`, `active`, `created_date`, `updated_date`) VALUES
(1, 'P1', 1, 1, '2021-10-14 09:03:02', '2021-10-14 09:15:17'),
(2, 'P2', 2, 1, '2021-10-14 09:04:59', '2021-10-14 09:15:23');

-- --------------------------------------------------------

--
-- Table structure for table `m_mantri`
--

CREATE TABLE `m_mantri` (
  `id` int(11) NOT NULL,
  `kode` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_mantri`
--

INSERT INTO `m_mantri` (`id`, `kode`, `nama`, `active`, `created_date`, `updated_date`) VALUES
(1, 1, 'Mantri 1', 1, '2021-10-11 23:30:55', '2021-10-11 23:30:55'),
(2, 2, 'Mantri 2', 1, '2021-10-11 23:33:57', '2021-10-11 23:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `m_menus`
--

CREATE TABLE `m_menus` (
  `menu_id` int(11) NOT NULL,
  `menu_nm` varchar(100) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `orders` decimal(10,2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_menus`
--

INSERT INTO `m_menus` (`menu_id`, `menu_nm`, `icon`, `color`, `url`, `orders`, `parent_id`, `active`, `keterangan`, `created_date`, `updated_date`) VALUES
(1, 'Dashboard', 'ni-shop text-primary', 'text-primary', 'dashboard', '1.00', NULL, 1, NULL, '2020-12-28 09:36:17', '2021-10-11 23:58:04'),
(2, 'Data Nasabah', 'ni-circle-08', 'text-orange', 'data_nasabah', '2.00', NULL, 1, NULL, '2021-10-08 01:21:40', '2021-10-12 00:03:48'),
(3, 'Kas Kecil', 'ni-books', 'text-yellow', 'kas_kecil', '3.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:07'),
(4, 'Storting', 'ni-send', 'text-default', 'storting', '4.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:25'),
(5, 'Droping', 'ni-curved-next', 'text-info', 'droping', '5.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:27'),
(6, 'Data Analisa', 'ni-sound-wave', 'text-pink', 'data_analisa', '6.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:34'),
(7, 'Laporan Keuangan', 'ni-money-coins', 'text-green', 'laporan_keuangan', '7.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-12 00:09:42'),
(8, 'Data Master', 'ni-box-2', NULL, '', '8.00', NULL, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(9, 'Perhitungan Bunga', NULL, NULL, 'perhitungan_bunga', '8.20', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(10, 'Perhitungan Asset Tetap', NULL, NULL, 'perhitungan_asset_tetap', '8.30', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(11, 'Data Master Leasing', NULL, NULL, 'data_leasing', '8.40', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(12, 'Data Nasabah', NULL, NULL, 'data_nasabah', '8.10', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(13, 'Data Master Vendor', NULL, NULL, 'data_vendor', '8.50', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08'),
(14, 'Data Master Karyawan', NULL, NULL, 'data_karyawan', '8.60', 8, 1, NULL, '2021-10-08 01:48:08', '2021-10-08 01:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `m_nasabah`
--

CREATE TABLE `m_nasabah` (
  `id` int(11) NOT NULL,
  `kode_nasabah` varchar(255) NOT NULL,
  `nama_nasabah` varchar(255) NOT NULL,
  `ktp` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_nasabah`
--

INSERT INTO `m_nasabah` (`id`, `kode_nasabah`, `nama_nasabah`, `ktp`, `status`) VALUES
(1, '123', 'JOKO AJI SAPUTRO', '00112233445566', ''),
(2, 'P1.01-0001', 'EKO WULANDARI', '1', ''),
(3, 'P1.01-0002', 'LAILA ALFIANTI', '2', 'RNT'),
(4, 'P1.01-0003', 'SO\'IMAH', '3', ''),
(5, 'P1.01-0004', 'ANIK ROHMIATI', '4', ''),
(6, 'P1.01-0005', 'SAROMAH', '5', ''),
(7, 'P1.01-0006', 'GIYEM', '6', ''),
(8, 'P1.01-0007', 'SITI ZUJAIDAH', '7', ''),
(9, 'P1.01-0008', 'YUSTITYA', '8', ''),
(10, 'P1.01-0009', 'MARINA/HADI', '9', ''),
(11, 'P1.01-0010', 'SISILIA S', '10', ''),
(12, 'P1.01-0011', 'AGUSTINA', '11', ''),
(13, 'P1.01-0012', 'SUPRIYANTI', '12', ''),
(14, 'P1.01-0013', 'WIWIK I', '13', ''),
(15, 'P1.01-0014', 'FATMAWATI', '14', ''),
(16, 'P1.01-0015', 'NURMIYATI', '15', ''),
(17, 'P1.01-0016', 'ANITA P', '16', ''),
(18, 'P1.01-0017', 'MOEKTI ARIYANTI', '17', ''),
(19, 'P1.01-0018', 'TUMINI', '18', ''),
(20, 'P1.01-0019', 'SETYOWATI', '19', ''),
(21, 'P1.01-0020', 'LIDYAWATI', '20', ''),
(22, 'P1.01-0021', 'KHUZANINAH', '21', ''),
(23, 'P1.01-0022', 'ROBI HARTATI', '22', ''),
(24, 'P1.01-0023', 'TRI RESTUWATI', '23', ''),
(25, 'P1.01-0024', 'WIWIK EKO S', '24', ''),
(26, 'P1.01-0025', 'WIJAYANTI', '25', ''),
(27, 'P1.01-0026', 'JOVITA H', '26', ''),
(28, 'P1.01-0027', 'OCTARIA T', '27', ''),
(29, 'P1.01-0028', 'R.A LAKSMIWATY', '28', ''),
(30, 'P1.01-0029', 'RISKI/SALMAH', '29', ''),
(31, 'P1.01-0030', 'MASRUCHAH', '30', ''),
(32, 'P1.01-0031', 'IIN INDRAWATI', '31', ''),
(33, 'P1.01-0032', 'SUPIYANI', '32', ''),
(34, 'P1.01-0033', 'JULAIKAH', '33', ''),
(35, 'P1.01-0034', 'FAAT SUPRIATI', '34', ''),
(36, 'P1.01-0035', 'MUJIATI', '35', ''),
(37, 'P1.01-0036', 'WIKI RATMI', '36', ''),
(38, 'P1.01-0037', 'MUSAWATI', '37', ''),
(39, 'P1.01-0038', 'SULASTRI', '38', ''),
(40, 'P1.01-0039', 'SAPTIANI', '39', ''),
(41, 'P1.01-0040', 'DIANA', '40', ''),
(42, 'P1.01-0041', 'SRI MUJIATI', '41', ''),
(43, 'P1.01-0042', 'DEWI IRAWATI', '42', ''),
(44, 'P1.01-0043', 'RINA DWI YANTI', '43', ''),
(45, 'P1.01-0044', 'ELLY PURNAMI', '44', ''),
(46, 'P1.01-0045', 'SUNANIK', '45', ''),
(47, 'P1.01-0046', 'SRI ASIH', '46', ''),
(48, 'P1.01-0047', 'NUR CHASANAH', '47', ''),
(49, 'P1.01-0048', 'IWAN HENDARTO', '48', ''),
(50, 'P1.01-0049', 'WIKANTI', '49', ''),
(51, 'P1.01-0050', 'RISKA DIANA I', '50', ''),
(52, 'P1.01-0051', 'SUMARNI', '51', ''),
(53, 'P1.01-0052', 'ANIK FITRI S', '52', ''),
(54, 'P1.01-0053', 'YULI PRAPTI D', '53', ''),
(55, 'P1.01-0054', 'AGNIS MULANDARI', '54', ''),
(56, 'P1.01-0055', 'IMROATUS SHOLIKA', '55', ''),
(57, 'P1.01-0056', 'MUDJIATI', '56', ''),
(58, 'P1.01-0057', 'SUPARTILAH', '57', ''),
(59, 'P1.01-0058', 'MUDJAROH', '58', ''),
(60, 'P1.01-0059', 'SITI MAISARO', '59', ''),
(61, 'P1.01-0060', 'WIWIN J', '60', ''),
(62, 'P1.01-0061', 'SUHARYATI', '61', ''),
(63, 'P1.01-0062', 'SUMARMI', '62', ''),
(64, 'P1.01-0063', 'SUTRINIL', '63', ''),
(65, 'P1.01-0064', 'SRIANI', '64', ''),
(66, 'P1.01-0065', 'TATIK A', '65', ''),
(67, 'P1.01-0066', 'ARI EKOWATI', '66', ''),
(68, 'P1.01-0067', 'SUKIRAH', '67', ''),
(69, 'P1.01-0068', 'R.R UMI PRASETYAWATI', '68', ''),
(70, 'P1.01-0069', 'DASWATI', '69', ''),
(71, 'P1.01-0070', 'MEIYATI WAHYUNI', '70', ''),
(72, 'P1.01-0071', 'HJ KUSMIYATI', '71', ''),
(73, 'P1.01-0072', 'INDRAWATI', '72', ''),
(74, 'P1.01-0073', 'KARTI', '73', ''),
(75, 'P1.01-0074', 'YUNANIK', '74', ''),
(76, 'P1.01-0075', 'SRI SUHARSIH', '75', ''),
(77, 'P1.01-0076', 'RIZKAH DRA', '76', ''),
(78, 'P1.01-0077', 'SOEWARDANI', '77', ''),
(79, 'P1.01-0078', 'SUKARTI', '78', ''),
(80, 'P1.01-0079', 'HERLY WAHYUDI', '79', ''),
(81, 'P1.01-0080', 'SUWATIN', '80', ''),
(82, 'P1.01-0081', 'WIWIK FARIDHA', '81', ''),
(83, 'P1.01-0082', 'WARTININGSIH', '82', ''),
(84, 'P1.01-0083', 'SUPRAPTI', '83', ''),
(85, 'P1.01-0084', 'MIMIN DWI ', '84', ''),
(86, 'P1.01-0085', 'RHISQI SHOFIA', '85', ''),
(87, 'P1.01-0086', 'ELSA MAULIDINA', '86', ''),
(88, 'P1.01-0087', 'FITRIAFI SETIANINGSIH', '87', ''),
(89, 'P1.01-0088', 'SITI ASRINI', '88', ''),
(90, 'P1.01-0089', 'NINIK AGUSTINI', '89', ''),
(91, 'P1.01-0090', 'ANI NUR A', '90', ''),
(92, 'P1.01-0091', 'SRI HARTINI', '91', ''),
(93, 'P1.01-0092', 'ANA DWI ARINI', '92', ''),
(94, 'P1.01-0093', 'SITI CHOTIJAH', '93', ''),
(95, 'P1.01-0094', 'TRIANI', '94', ''),
(96, 'P1.01-0095', 'ASNIFAH', '95', ''),
(97, 'P1.01-0096', 'ENDANG P', '96', ''),
(98, 'P1.01-0097', 'MARKILAH', '97', ''),
(99, 'P1.01-0098', 'SUNARNING', '98', ''),
(100, 'P1.01-0099', 'SRI PUJI ASTUTIK', '99', ''),
(101, 'P1.01-0100', 'CHONIKIDA LAILAH', '100', ''),
(102, 'P1.01-0101', 'MUJAYANA', '101', ''),
(103, 'P1.01-0102', 'SUMINI', '102', ''),
(104, 'P1.01-0103', 'NUR WALIMAH', '103', ''),
(105, 'P1.01-0104', 'LINDA SULISTYAWATI', '104', ''),
(106, 'P1.01-0105', 'NURUL AGUSTINA', '105', ''),
(107, 'P1.01-0106', 'FITRI DWI A.S', '106', ''),
(108, 'P1.01-0107', 'NUR MAULIDIYAH', '107', ''),
(109, 'P1.01-0108', 'LASMI', '108', ''),
(110, 'P1.01-0109', 'MARIA D.P', '109', ''),
(111, 'P1.01-0110', 'MARPUAH', '110', ''),
(112, 'P1.01-0111', 'KHUSNUL K', '111', ''),
(113, 'P1.01-0112', 'ANIS IRAWATI', '112', ''),
(114, 'P1.01-0113', 'SHOLIKAH', '113', ''),
(115, 'P1.01-0114', 'SUWARTI', '114', ''),
(116, 'P1.01-0115', 'YULI T', '115', ''),
(117, 'P1.01-0116', 'DIAN T', '116', ''),
(118, 'P1.01-0117', 'RICA ANA', '117', ''),
(119, 'P1.01-0118', 'ASMAUL KHUSNA', '118', ''),
(120, 'P1.01-0119', 'TRINISA', '119', ''),
(121, 'P1.01-0120', 'SOEWARDANI', '120', ''),
(122, 'P1.01-0121', 'LILIS', '121', ''),
(123, 'P1.01-0122', 'ROFIAH', '122', ''),
(124, 'P1.01-0123', 'LILIK', '123', ''),
(125, 'P1.01-0124', 'MELA O', '124', ''),
(126, 'P1.01-0125', 'TITIK O', '125', ''),
(127, 'P1.01-0126', 'INDRAYANI', '126', ''),
(128, 'P1.01-0127', 'SITI AMINAH', '127', ''),
(129, 'P1.01-0128', 'RUSMINI', '128', ''),
(130, 'P1.01-0129', 'DWI PURWANTI', '129', ''),
(131, 'P1.01-0130', 'MURTININGSIH', '130', ''),
(132, 'P1.01-0131', 'SITI URIPAH', '131', ''),
(133, 'P1.01-0132', 'LILIK SURYANTI', '132', ''),
(134, 'P1.01-0133', 'EDITA M', '133', ''),
(135, 'P1.01-0134', 'SRIWANTI', '134', ''),
(136, 'P1.01-0135', 'MARBIAH', '135', ''),
(137, 'P1.01-0136', 'SURATI', '136', ''),
(138, 'P1.01-0137', 'NO NAME', '137', ''),
(139, 'P1.01-0138', 'SUGIATI', '138', ''),
(140, 'P1.01-0139', 'MUJI A', '139', ''),
(141, 'P1.01-0140', 'SRI WILIS', '140', ''),
(142, 'P1.01-0141', 'LILIEK L', '141', ''),
(143, 'P1.01-0142', 'DEWI ASIAH', '142', ''),
(144, 'P1.01-0143', 'RIYANTI', '143', ''),
(145, 'P1.01-0144', 'WINDI E', '144', ''),
(146, 'P1.01-0145', 'MUSRINIATI', '145', ''),
(147, 'P1.01-0146', 'LULUK ', '146', ''),
(148, 'P1.01-0147', 'SAUWAMAH', '147', ''),
(149, 'P1.01-0148', 'SUNARIYAH', '148', ''),
(150, 'P1.01-0149', 'MUJIYAH', '149', ''),
(151, 'P1.01-0150', 'MARWAH', '150', ''),
(152, 'P1.01-0151', 'NETI N', '151', ''),
(153, 'P1.01-0152', 'SUBANDI', '152', ''),
(154, 'P1.01-0153', 'SANIK', '153', ''),
(155, 'P1.01-0154', 'ZUBAIDAH', '154', ''),
(156, 'P1.01-0155', 'IDA RATNA', '155', ''),
(157, 'P1.01-0156', 'DWI INDRA', '156', ''),
(158, 'P1.01-0157', 'LULUK DIYAH', '157', ''),
(159, 'P1.01-0158', 'SUWARTI', '158', ''),
(160, 'P1.01-0159', 'SRIATUN', '159', ''),
(161, 'P1.01-0160', 'MARPUAH', '160', ''),
(162, 'P1.01-0161', 'WIWIK YULIATI', '161', ''),
(163, 'P1.01-0162', 'HOTIMAH', '162', ''),
(164, 'P1.01-0163', 'IS INDRAWATI', '163', ''),
(165, 'P1.01-0164', 'MINARSIH', '164', ''),
(166, 'P1.01-0165', 'UMI SINTA', '165', ''),
(167, 'P1.01-0166', 'VIVIN', '166', ''),
(168, 'P1.01-0167', 'UMI ', '167', ''),
(169, 'P1.01-0168', 'SULASMI', '168', ''),
(170, 'P1.01-0169', 'NO NAME', '169', ''),
(171, 'P1.01-0170', 'SITI HARNANIK', '170', ''),
(172, 'P1.01-0171', 'ERNA W', '171', ''),
(173, 'P1.01-0172', 'UMI', '172', ''),
(174, 'P1.01-0173', 'TIA', '173', ''),
(175, 'P1.01-0174', 'YUYUN', '174', ''),
(176, 'P1.01-0175', 'SATUKI/S', '175', ''),
(177, 'P1.01-0176', 'SUMILAH', '176', ''),
(178, 'P1.01-0177', 'FITRI S', '177', ''),
(179, 'P1.01-0178', 'MURTALIB', '178', ''),
(180, 'P1.01-0179', 'DELVITA', '179', ''),
(181, 'P1.01-0180', 'ERVINA D', '180', ''),
(182, 'P1.01-0181', 'TAUFIK', '181', ''),
(183, 'P1.01-0182', 'KHOIRIYAH', '182', ''),
(184, 'P1.01-0183', 'ANA AMBARWATI', '183', ''),
(185, 'P1.01-0184', 'PURIYANA', '184', ''),
(186, 'P1.01-0185', 'YOASIH NOVARIA', '185', ''),
(187, 'P1.01-0186', 'TRI OKTAVIA', '186', ''),
(188, 'P1.01-0187', 'SITI ROMLAH', '187', ''),
(189, 'P1.01-0188', 'NO NAME', '188', ''),
(190, 'P1.01-0189', 'AINI ADIMAH', '189', ''),
(191, 'P1.01-0190', 'PURNA HARINI', '190', ''),
(192, 'P1.01-0191', 'PRADAH', '191', ''),
(193, 'P1.01-0192', 'MARIA MAGDA', '192', ''),
(194, 'P1.01-0193', 'KUSMIATI', '193', ''),
(195, 'P1.01-0194', 'SOENARMI', '194', ''),
(196, 'P1.01-0195', 'ERMA 2', '195', ''),
(197, 'P1.01-0196', 'ENDANG P', '196', ''),
(198, 'P1.01-0197', 'NINIS EKO', '197', ''),
(199, 'P1.01-0198', 'ENDAH', '198', ''),
(200, 'P1.01-0199', 'NURALIFAH', '199', ''),
(201, 'P1.01-0200', 'KASANAH', '200', ''),
(202, 'P1.01-0201', 'TOBERI', '201', ''),
(203, 'P1.01-0202', 'NUR AZIZAH', '202', ''),
(204, 'P1.01-0203', 'SUTALNI', '203', ''),
(205, 'P1.01-0204', 'UMI CHOTIMAH', '204', ''),
(206, 'P1.01-0205', 'ARIFAH', '205', ''),
(207, 'P1.01-0206', 'WARNI', '206', ''),
(208, 'P1.01-0207', 'IMROATUS ', '207', ''),
(209, 'P1.01-0208', 'NOVITA INDRA', '208', ''),
(210, 'P1.01-0209', 'SIYAMAH', '209', ''),
(211, 'P1.01-0210', 'ANIS WAHYU', '210', ''),
(212, 'P1.01-0211', 'SITI AMIDAH', '211', ''),
(213, 'P1.01-0212', 'MOHAMMAD', '212', ''),
(214, 'P1.01-0213', 'LILIK JULIATI', '213', ''),
(215, 'P1.01-0214', 'SUTIJO', '214', ''),
(216, 'P1.01-0215', 'SUTRIS T', '215', ''),
(217, 'P1.01-0216', 'JANEM S', '216', ''),
(218, 'P1.01-0217', 'FITRIA', '217', ''),
(219, 'P1.01-0218', 'NO NAME', '218', ''),
(220, 'P1.01-0219', 'ASRI WAHYUNI', '219', ''),
(221, 'P1.01-0220', 'SRI HAYATI', '220', ''),
(222, 'P1.01-0221', 'ANIK SETYAWATI', '221', ''),
(223, 'P1.01-0222', 'SUMATI', '222', ''),
(224, 'P1.01-0223', 'HENI P', '223', ''),
(225, 'P1.01-0224', 'IRAWATI', '224', ''),
(226, 'P1.01-0225', 'HAJI KOMARIYAH', '225', ''),
(227, 'P1.01-0226', 'MARPURYANTO', '226', ''),
(228, 'P1.01-0227', 'ENI PURWATI', '227', ''),
(229, 'P1.01-0228', 'ARNOIRAWATI', '228', ''),
(230, 'P1.01-0229', 'SITI ASIYAH', '229', ''),
(231, 'P1.01-0230', 'AINUN', '230', ''),
(232, 'P1.01-0231', 'NOEKE', '231', ''),
(233, 'P1.01-0232', 'ANTIFITRIAH', '232', ''),
(234, 'P1.01-0233', 'CUSNUL', '233', ''),
(235, 'P1.01-0234', 'ISTIANAH', '234', ''),
(236, 'P1.01-0235', 'DEWI', '235', ''),
(237, 'P1.01-0236', 'SUSUANA', '236', ''),
(238, 'P1.01-0237', 'NANCI', '237', ''),
(239, 'P1.01-0238', 'BAYU', '238', ''),
(240, 'P1.01-0239', 'EKA', '239', ''),
(241, 'P1.01-0240', 'SUMIJAH', '240', ''),
(242, 'P1.01-0241', 'INDASAH', '241', ''),
(243, 'P1.01-0242', 'ROMLAH', '242', ''),
(244, 'P1.01-0243', 'DARMI', '243', ''),
(245, 'P1.01-0244', 'REYNAWATI', '244', ''),
(246, 'P1.01-0245', 'EKANANIK', '245', ''),
(247, 'P1.01-0246', 'SITI ZAENAB', '246', ''),
(248, 'P1.01-0247', 'SITI AISYAH', '247', ''),
(249, 'P1.01-0248', 'WENI', '248', ''),
(250, 'P1.01-0249', 'SRI ERMAWATI', '249', ''),
(251, 'P1.01-0250', 'SINTA NURI', '250', ''),
(252, 'P1.01-0251', 'YATI', '251', ''),
(253, 'P1.01-0252', 'SUPRIATIN', '252', ''),
(254, 'P1.01-0253', 'TIMAH', '253', ''),
(255, 'P1.01-0254', 'SUPJIATI', '254', ''),
(256, 'P1.01-0255', 'SAMINAH', '255', ''),
(257, 'P1.01-0256', 'RISCHA R', '256', ''),
(258, 'P1.01-0257', 'SRI WAHYUNI', '257', ''),
(259, 'P1.01-0258', 'ISTIQOMAH', '258', ''),
(260, 'P1.01-0259', 'EKA VIRGIAN', '259', ''),
(261, 'P1.01-0260', 'SITI MARIA', '260', ''),
(262, 'P1.01-0261', 'SITI AMINAH', '261', ''),
(263, 'P1.01-0262', 'HENI ANGGRAENI', '262', ''),
(264, 'P1.01-0263', 'SUSIANI', '263', ''),
(265, 'P1.01-0264', 'LULUK NOVIASIH', '264', ''),
(266, 'P1.01-0265', 'NURKASANAH', '265', ''),
(267, 'P1.01-0266', 'AILAH', '266', ''),
(268, 'P1.01-0267', 'NOVI NUR A', '267', ''),
(269, 'P1.01-0268', 'NUR HAYATI', '268', ''),
(270, 'P1.01-0269', 'MUJI TITIK', '269', ''),
(271, 'P1.01-0270', 'MASRUCHAH', '270', ''),
(272, 'P1.01-0271', 'RENI AGUSTYA', '271', ''),
(273, 'P1.01-0272', 'SULIS HANDAYANI', '272', ''),
(274, 'P1.01-0273', 'EKA AGUSTINE', '273', ''),
(275, 'P1.01-0274', 'LULUK MASLUKHATIN', '274', ''),
(276, 'P1.01-0275', 'SUENI LAILATI', '275', ''),
(277, 'P1.01-0276', 'WIDIA T', '276', ''),
(278, 'P1.01-0277', 'YULITA', '277', ''),
(279, 'P1.01-0278', 'LISWATULLAH', '278', ''),
(280, 'P1.01-0279', 'TITIK ZULFAH', '279', ''),
(281, 'P1.01-0280', 'SUKRIYAH', '280', ''),
(282, 'P1.01-0281', 'HARTINI', '281', ''),
(283, 'P1.01-0282', 'SUSIYANI', '282', ''),
(284, 'P1.01-0283', 'SITI MARYAM', '283', ''),
(285, 'P1.01-0284', 'ESTY PUJI RAHAYU', '284', ''),
(286, 'P1.01-0285', 'ALBERTA ANITA', '285', ''),
(287, 'P1.01-0286', 'RINA MUTIA', '286', ''),
(288, 'P1.01-0287', 'NOVITASARI', '287', ''),
(289, 'P1.01-0288', 'SITI FATIMAH', '288', ''),
(290, 'P1.01-0289', 'ROHMATIN', '289', ''),
(291, 'P1.01-0290', 'ITA AMALIA', '290', ''),
(292, 'P1.01-0291', 'DESY ANGGRAINI', '291', ''),
(293, 'P1.01-0292', 'TRIA LESTARI', '292', ''),
(294, 'P1.01-0293', 'SUHARINI', '293', ''),
(295, 'P1.01-0294', 'MURYATI', '294', ''),
(296, 'P1.01-0295', 'ISMI AYUSARI', '295', ''),
(297, 'P1.01-0296', 'LULUK WIJIATI', '296', ''),
(298, 'P1.01-0297', 'SITI AMINAH', '297', ''),
(299, 'P1.01-0298', 'SAPTINING S', '298', ''),
(300, 'P1.01-0299', 'JUWARIYAH', '299', ''),
(301, 'P1.01-0300', 'DWI INDAH', '300', ''),
(302, 'P1.01-0301', 'HENI PURNAMASARI', '301', ''),
(303, 'P1.01-0302', 'ASMAWATI', '302', ''),
(304, 'P1.01-0303', 'SRI MARIANINGSIH', '303', ''),
(305, 'P1.01-0304', 'SULASIH', '304', ''),
(306, 'P1.01-0305', 'NINIK S', '305', ''),
(307, 'P1.01-0306', 'ASIK', '306', ''),
(308, 'P1.01-0307', 'ATIK NUR ALANG', '307', ''),
(309, 'P1.01-0308', 'NINGSIH', '308', ''),
(310, 'P1.01-0309', 'SUPARMI', '309', ''),
(311, 'P1.01-0310', 'PUJIATI', '310', ''),
(312, 'P1.01-0311', 'TUTIK HARYANTI', '311', ''),
(313, 'P1.01-0312', 'DITA MARCYA', '312', ''),
(314, 'P1.01-0313', 'SRI UTAMI', '313', ''),
(315, 'P1.01-0314', 'WULANSARI', '314', ''),
(316, 'P1.01-0315', 'LAKSMI W', '315', ''),
(317, 'P1.01-0316', 'ENDANG A.S', '316', ''),
(318, 'P1.01-0317', 'ENDAH', '317', ''),
(319, 'P1.01-0318', 'YUNIASIH', '318', ''),
(320, 'P1.01-0319', 'RUSMININGSIH', '319', ''),
(321, 'P1.01-0320', 'MOERIN', '320', ''),
(322, 'P1.01-0321', 'H. HAYATI', '321', ''),
(323, 'P1.01-0322', 'NINING FITRIA', '322', ''),
(324, 'P1.01-0323', 'SUKARTI', '323', ''),
(325, 'P1.01-0324', 'KUSRINI', '324', ''),
(326, 'P1.01-0325', 'TITIK NURWANI', '325', ''),
(327, 'P1.01-0326', 'RATNA TRI', '326', ''),
(328, 'P1.01-0327', 'FENTY AYU PUSPITA', '327', ''),
(329, 'P1.01-0328', 'HOTIJAH', '328', ''),
(330, 'P1.01-0329', 'SRIKAT MUJIATIK', '329', ''),
(331, 'P1.01-0330', 'HJ.AINUN SIFA', '330', ''),
(332, 'P1.01-0331', 'ANITA ROCHMAWATI', '331', ''),
(333, 'P1.01-0332', 'CHUSUMAWATI', '332', ''),
(334, 'P1.01-0333', 'SARAS SAPTUTI', '333', ''),
(335, 'P1.01-0334', 'SIHANI', '334', ''),
(336, 'P1.01-0335', 'PAENAH', '335', ''),
(337, 'P1.01-0336', 'NUR WAHYUNI', '336', ''),
(338, 'P1.01-0337', 'SUWARTI', '337', ''),
(339, 'P1.01-0338', 'KANTI WINARSIH', '338', ''),
(340, 'P1.01-0339', 'MUDAAH KUSWANTI', '339', ''),
(341, 'P1.01-0340', 'UMI FITRIAH', '340', ''),
(342, 'P1.01-0341', 'SUHARTATIK', '341', ''),
(343, 'P1.01-0342', 'INDRASARI', '342', ''),
(344, 'P1.01-0343', 'RINI AGUSTINA', '343', ''),
(345, 'P1.01-0344', 'SETYARINI P', '344', ''),
(346, 'P1.01-0345', 'SRIANAH', '345', ''),
(347, 'P1.01-0346', 'ANISAH', '346', ''),
(348, 'P1.01-0347', 'HEFI ROHMA\'ATI', '347', ''),
(349, 'P1.01-0348', 'MAILATUL CHOIRIYAH', '348', ''),
(350, 'P1.01-0349', 'MAROTIN', '349', ''),
(351, 'P1.01-0350', 'TITIK ARIYANI', '350', ''),
(352, 'P1.01-0351', 'TRISNA R', '351', ''),
(353, 'P1.01-0352', 'SITI NURSIYAH', '352', ''),
(354, 'P1.01-0353', 'SUSILOWATI', '353', ''),
(355, 'P1.01-0354', 'MUTIANA', '354', ''),
(356, 'P1.01-0355', 'IKA PUMUHARTYANING', '355', ''),
(357, 'P1.01-0356', 'MASRUROH', '356', ''),
(358, 'P1.01-0357', 'MISKANAH', '357', ''),
(359, 'P1.01-0358', 'EMDI RIYONO', '358', ''),
(360, 'P1.01-0359', 'SUMARIYATI N', '359', ''),
(361, 'P1.01-0360', 'PRATIWI', '360', ''),
(362, 'P1.01-0361', 'SUCIATI', '361', ''),
(363, 'P1.01-0362', 'SUNARTI', '362', ''),
(364, 'P1.01-0363', 'ANI HARTAMI', '363', ''),
(365, 'P1.01-0364', 'SUMARLIK', '364', ''),
(366, 'P1.01-0365', 'NUR HAYATI', '365', ''),
(367, 'P1.01-0366', 'LIYAH UMAMI', '366', ''),
(368, 'P1.01-0367', 'FADILAH', '367', ''),
(369, 'P1.01-0368', 'SITI MARIA U', '368', ''),
(370, 'P1.01-0369', 'SA\'ADAH', '369', ''),
(371, 'P1.01-0370', 'SURYANI', '370', ''),
(372, 'P1.01-0371', 'LUSIA TANTRI', '371', ''),
(373, 'P1.01-0372', 'SRICE SRI UTAMI', '372', ''),
(374, 'P1.01-0373', 'SUMINAH', '373', ''),
(375, 'P1.01-0374', 'SITI AISAH', '374', ''),
(376, 'P1.01-0375', 'SUHARTINI', '375', ''),
(377, 'P1.01-0376', 'SUSIATI', '376', ''),
(378, 'P1.01-0377', 'NETI', '377', ''),
(379, 'P1.01-0378', 'UMU SALMANI', '378', ''),
(380, 'P1.01-0379', 'ANITA ELERINA', '379', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_nasabah_fix`
--

CREATE TABLE `m_nasabah_fix` (
  `id` int(11) NOT NULL,
  `nama_nasabah` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tipe_nasabah` varchar(5) NOT NULL DEFAULT 'N',
  `kode_pos_id` int(11) NOT NULL,
  `mantri_id` int(11) NOT NULL,
  `running_number` int(25) NOT NULL,
  `kayawan_id` int(11) NOT NULL,
  `nik` int(25) NOT NULL,
  `kk` int(25) NOT NULL,
  `alamat_ktp` varchar(255) NOT NULL,
  `alamat_tinggal` varchar(255) NOT NULL,
  `AR` int(11) DEFAULT NULL,
  `NT` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_nasabah_fix`
--

INSERT INTO `m_nasabah_fix` (`id`, `nama_nasabah`, `tipe_nasabah`, `kode_pos_id`, `mantri_id`, `running_number`, `kayawan_id`, `nik`, `kk`, `alamat_ktp`, `alamat_tinggal`, `AR`, `NT`, `active`, `created_date`, `updated_date`) VALUES
(1, 'NASABAH 11', 'N', 1, 1, 1, 2, 3121231, 124123, '123123', '', NULL, NULL, 1, '2021-10-16 09:55:07', '2021-10-16 12:29:01'),
(3, 'NASABAH 2', 'N', 1, 1, 2, 0, 2222, 2222, 'jl.2            ', 'jl.2            ', NULL, NULL, 0, '2021-10-17 17:48:02', '2021-10-17 17:48:34'),
(4, 'NASABAH 3', 'N', 1, 2, 3, 0, 33333333, 2147483647, 'jl.3            ', 'jl.3', NULL, NULL, 0, '2021-10-17 17:52:01', '2021-10-17 17:53:12'),
(5, 'NASABAH 4', 'N', 1, 2, 4, 0, 444444444, 2147483647, 'jl.4            ', 'jl.4', NULL, NULL, 1, '2021-10-17 17:53:29', '2021-10-17 17:59:10'),
(6, 'Nasabah 5', 'N', 1, 2, 5, 0, 2147483647, 2147483647, 'asdkm            ', 'askdm            ', NULL, NULL, 1, '2021-10-17 18:08:21', '2021-10-17 18:08:21');

-- --------------------------------------------------------

--
-- Table structure for table `t_droping`
--

CREATE TABLE `t_droping` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_trans` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kode_nasabah` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nominal` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_droping`
--

INSERT INTO `t_droping` (`id`, `tgl`, `no_trans`, `kode_nasabah`, `nominal`, `update_date`, `create_date`) VALUES
(1, '1970-01-01', 'DRP.09.002', 'P1.01-0002', 100000, '2021-10-18 13:48:35', '2021-10-12 04:51:37'),
(2, '1970-01-01', '123', '123', 123, '2021-10-13 16:04:10', '2021-10-13 16:04:10'),
(3, '2021-10-12', '123', '123', 3, '2021-10-13 16:08:35', '2021-10-13 16:08:35'),
(4, '2021-10-06', '123', '123', 123, '2021-10-13 16:10:57', '2021-10-13 16:10:57'),
(5, '2021-10-14', 'DRP.10.001', 'P1.01-0266', 650000, '2021-10-14 15:03:24', '2021-10-14 15:03:24');

-- --------------------------------------------------------

--
-- Table structure for table `t_storting`
--

CREATE TABLE `t_storting` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_trans` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kode_nasabah` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nominal` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_storting`
--

INSERT INTO `t_storting` (`id`, `tgl`, `no_trans`, `kode_nasabah`, `nominal`, `update_date`, `create_date`, `status`) VALUES
(1, '2021-10-14', 'STR.09-001', 'P1.01-0002', 65000, '2021-10-15 06:35:51', '2021-10-14 16:12:23', 'NT'),
(2, '2021-10-14', 'STR.10.001', 'P1.01-0054', 100000, '2021-10-14 16:37:04', '2021-10-14 16:37:04', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `tipe_karyawan` varchar(1) DEFAULT 'K',
  `kode_pos` int(11) DEFAULT NULL,
  `running_number` int(11) DEFAULT NULL,
  `mantri_id` int(11) DEFAULT NULL,
  `nama_karyawan` varchar(100) NOT NULL,
  `nik` int(50) NOT NULL,
  `kk` int(50) NOT NULL,
  `alamat_ktp` varchar(255) NOT NULL,
  `alamat_tinggal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `tipe_karyawan`, `kode_pos`, `running_number`, `mantri_id`, `nama_karyawan`, `nik`, `kk`, `alamat_ktp`, `alamat_tinggal`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1634542124, 1, NULL, NULL, NULL, NULL, 'K', 1, 0, 1, 'Administrator', 111111111, 111111112, 'Jl.X', 'Jl.X'),
(2, '::1', 'karyawan1', '$2y$08$T38EHDrPXzuHTBsnU1qTy.b0dhSsjfdOWSKNvOXkuDqqJdClxRxTy', NULL, '', NULL, NULL, NULL, NULL, 1634197586, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 1, 1, 'karyawan no1', 3113, 3131, 'jl.k1', 'jl.k1'),
(3, '::1', 'karyawan2', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 2, 2, 'Karyawan no2', 312312, 123123123, 'jl.k2', 'jl.k2'),
(4, '::1', 'karyawan3', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 3, 2, 'Karyawan no3', 312312, 123123123, 'jl.k2', 'jl.k2'),
(5, '::1', 'karyawan4', '$2y$08$T38EHDrPXzuHTBsnU1qTy.b0dhSsjfdOWSKNvOXkuDqqJdClxRxTy', NULL, '', NULL, NULL, NULL, NULL, 1634197586, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 1, 1, 'karyawan no4', 3113, 3131, 'jl.k1', 'jl.k1'),
(6, '::1', 'karyawan5', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 2, 2, 'Karyawan no5', 312312, 123123123, 'jl.k2', 'jl.k2'),
(7, '::1', 'karyawan6', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 3, 2, 'Karyawan no6', 312312, 123123123, 'jl.k2', 'jl.k2'),
(8, '::1', 'karyawan7', '$2y$08$T38EHDrPXzuHTBsnU1qTy.b0dhSsjfdOWSKNvOXkuDqqJdClxRxTy', NULL, '', NULL, NULL, NULL, NULL, 1634197586, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 1, 1, 'karyawan no7', 3113, 3131, 'jl.k1', 'jl.k1'),
(9, '::1', 'karyawan8', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 2, 2, 'Karyawan no8', 312312, 123123123, 'jl.k2', 'jl.k2'),
(10, '::1', 'karyawan9', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 3, 2, 'Karyawan no9', 312312, 123123123, 'jl.k2', 'jl.k2'),
(11, '::1', 'karyawan10', '$2y$08$T38EHDrPXzuHTBsnU1qTy.b0dhSsjfdOWSKNvOXkuDqqJdClxRxTy', NULL, '', NULL, NULL, NULL, NULL, 1634197586, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 1, 1, 'karyawan no10', 3113, 3131, 'jl.k1', 'jl.k1'),
(12, '::1', 'karyawan11', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 2, 2, 'Karyawan no11', 312312, 123123123, 'jl.k2', 'jl.k2'),
(13, '::1', 'karyawan12', '$2y$08$jM4lYr2YrnpRvXhLTj0TVeoh5G0aZrXg1N55z8klULwyXjW4dbeIq', NULL, '', NULL, NULL, NULL, NULL, 1634197746, NULL, 1, NULL, NULL, NULL, NULL, 'K', 1, 3, 2, 'Karyawan no12', 312312, 123123123, 'jl.k2', 'jl.k2');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `__m_r_mantri_karyawan`
--

CREATE TABLE `__m_r_mantri_karyawan` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `mantri_id` mediumint(8) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `__m_r_mantri_karyawan`
--

INSERT INTO `__m_r_mantri_karyawan` (`id`, `user_id`, `mantri_id`, `active`, `created_date`, `updated_date`) VALUES
(1, 1, 1, 1, '2021-10-11 23:53:58', '2021-10-11 23:53:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menu` (`fk_id_menu`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_kode_pos`
--
ALTER TABLE `m_kode_pos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_mantri`
--
ALTER TABLE `m_mantri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_menus`
--
ALTER TABLE `m_menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `m_nasabah`
--
ALTER TABLE `m_nasabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_nasabah_fix`
--
ALTER TABLE `m_nasabah_fix`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`),
  ADD UNIQUE KEY `running_number` (`running_number`),
  ADD KEY `fk_nasabah_mantri` (`mantri_id`),
  ADD KEY `fk_nasabah_kode_pos` (`kode_pos_id`);

--
-- Indexes for table `t_droping`
--
ALTER TABLE `t_droping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_storting`
--
ALTER TABLE `t_storting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_mantri` (`mantri_id`),
  ADD KEY `fk_users_kode_pos` (`kode_pos`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `__m_r_mantri_karyawan`
--
ALTER TABLE `__m_r_mantri_karyawan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_mantri_karyawan` (`user_id`,`mantri_id`) USING BTREE,
  ADD KEY `fk_mantri_idx` (`user_id`) USING BTREE,
  ADD KEY `fk_m_r_karyawan_idx` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_kode_pos`
--
ALTER TABLE `m_kode_pos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_mantri`
--
ALTER TABLE `m_mantri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_menus`
--
ALTER TABLE `m_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `m_nasabah`
--
ALTER TABLE `m_nasabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `m_nasabah_fix`
--
ALTER TABLE `m_nasabah_fix`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_droping`
--
ALTER TABLE `t_droping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_storting`
--
ALTER TABLE `t_storting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `__m_r_mantri_karyawan`
--
ALTER TABLE `__m_r_mantri_karyawan`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD CONSTRAINT `fk_menu` FOREIGN KEY (`fk_id_menu`) REFERENCES `m_menus` (`menu_id`);

--
-- Constraints for table `m_nasabah_fix`
--
ALTER TABLE `m_nasabah_fix`
  ADD CONSTRAINT `fk_nasabah_kode_pos` FOREIGN KEY (`kode_pos_id`) REFERENCES `m_kode_pos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_nasabah_mantri` FOREIGN KEY (`mantri_id`) REFERENCES `m_mantri` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_kode_pos` FOREIGN KEY (`kode_pos`) REFERENCES `m_kode_pos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_users_mantri` FOREIGN KEY (`mantri_id`) REFERENCES `m_mantri` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
