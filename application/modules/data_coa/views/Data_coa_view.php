<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Data COA</h6>

        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" data-toggle="modal" data-target="#modal-input" class="btn btn-sm btn-neutral">Tambah COA</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">

  <div class='card'>
    <div class='card-header'>
      <h4 class='card-title m-0'>Data COA</h4>
      <button id="btn-show-all-children" type="button">Expand All</button>
    </div>

    <div class='card-body table-responsive'>
      <table id="tabel_coa" class="table table-bordered text-nowrap">
        <thead class="thead-light">
          <tr>
            <th width="1%"></th>
            <th>id</th>
            <th>nama</th>
          </tr>
        </thead>
      </table>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-sm-12 col-md-5" id="dataTable_showing">

        </div>
        <div class="col-sm-12 col-md-7" id="dataTable_paginate">

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>
<!-- JS -->
<script>
  $(function() {
    $(document).ready(function() {
      list_coa();
      // $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
    });
  });

  function list_coa() {
    var table = $('#tabel_coa').DataTable({
      "ajax": '<?php echo base_url() ?>data_coa/get_coa1?tipe=json',
      "order": [
        [1, "asc"]
      ],
      "language": {
        "paginate": {
          "next": '<i class="fas fa-angle-right"></i>',
          "previous": '<i class="fas fa-angle-left"></i>',
        }
      },
      initComplete: (settings, json) => {
        $("#dataTable_showing").append($(".dataTables_info"));
        $("#dataTable_paginate").append($(".dataTables_paginate"));
        $(".dataTables_paginate").addClass('float-right')
      },
      "columns": [{
          className: 'details-control',
          orderable: false,
          data: null,
          defaultContent: '',
        },
        {
          "data": 'kode_coa',
          "className": 'align-middle text-nowrap text-left',
          "width": '1%'

        },
        {
          "data": 'description',
          "className": 'align-middle text-nowrap',
          "width": '30%'

        },

      ],
      columnDefs: [{
          targets: [1, 2],
          className: "fee-col"
        },
        {
          targets: [0],
          className: "label-col"
        }
      ],
    });
    $('#btn-show-all-children').on('click', function() {
      // Expand row details
      table.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
    });

    function format(rowData) {
      console.log(rowData)
      var childTable = '<table id="tabelcoa2' + rowData.kode_coa + '" class="table table-bordered nowrap w-100" width="100%">' +
        '<thead style="display:none"></thead >' +
        '</table>';
      return $(childTable).toArray();
    }

    function format2(rowData) {
      console.log(rowData)
      var childTable = '<table id="tabelcoa3' + rowData.kode_coa + '" class="table table-bordered text-nowrap " width="100%">' +
        '<thead style="display:none"></thead >' +
        '</table>';
      return $(childTable).toArray();
    }

    function format3(rowData) {
      console.log(rowData)
      var childTable = '<table id="tabelcoa4' + rowData.kode_coa + '" class="display compact wrap w-100 cell-border" width="100%">' +
        '<thead style="display:none"></thead >' +
        '</table>';
      return $(childTable).toArray();
    }
    $('#tabel_coa tbody').on('click', 'td.details-control', function() {
      var tr = $(this).closest('tr');
      var row = table.row(tr);
      var rowData = row.data();


      if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');

        // Destroy the Child Datatable
        $('#tabelcoa2' + rowData.kode_coa).DataTable().destroy();
      } else {
        // Open this row
        row.child(format(rowData)).show();
        var id = rowData.kode_coa;
        console.log(rowData.kode_coa);

        childTable = $('#tabelcoa2' + id).DataTable({
          dom: "t",
          ajax: {
            url: '<?php echo base_url() ?>data_coa/get_coa2',
            data: {
              id: id
            },
          },
          columns: [{
              className: 'details-control1',
              orderable: false,
              data: null,
              defaultContent: '',
              width: '1%'
            },
            {
              data: "kode_coa",
              className: 'align-middle text-nowrap text-left',
              width: '1%'
            },
            {
              data: "description",
              className: 'align-middle text-nowrap text-center',
              width: '30%'
            },
          ],
          columnDefs: [{
              targets: [1, 2],
              className: "fee-col"
            },
            {
              targets: [0],
              className: "label-col"
            }
          ],
          select: false,
        });

        tr.addClass('shown');
      }
    });

    $('#tabel_coa tbody').on('click', 'td.details-control1', function() {
      var tr = $(this).closest('tr');
      var row = childTable.row(tr);
      var rowData = row.data();


      if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');

        // Destroy the Child Datatable
        $('#tabelcoa3' + rowData.kode_coa).DataTable().destroy();
      } else {
        // Open this row
        row.child(format2(rowData)).show();
        var id = rowData.kode_coa;


        childTable2 = $('#tabelcoa3' + id).DataTable({
          dom: "t",
          ajax: {
            url: '<?php echo base_url() ?>data_coa/get_coa3',
            data: {
              kode_coa_2: rowData.kode_coa,
              // kode_coa_2: rowData.kode_coa_2,
            },
          },
          columns: [{
              className: 'details-control2',
              orderable: false,
              data: null,
              defaultContent: ''
            },
            {
              data: "kode_coa"
            },
            {
              data: "description"
            }
          ],
          columnDefs: [{
              targets: [1, 2],
              className: "fee-col"
            },
            {
              targets: [0],
              className: "label-col3"
            }
          ],
          select: false,
        });

        tr.addClass('shown');
      }
    });
    $('tbody').on('click', 'td.details-control2', function() {
      var tr = $(this).closest('tr');
      var row = childTable2.row(tr);
      var rowData = row.data();
      console.log(rowData.invoice);

      if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');

        // Destroy the Child Datatable
        $('#tabelcoa4' + rowData.kode_coa).DataTable().destroy();
      } else {
        // Open this row
        row.child(format3(rowData)).show();
        var id = rowData.kode_coa;


        childTable3 = $('#tabelcoa4' + id).DataTable({
          dom: "t",
          ajax: {
            url: '<?php echo base_url() ?>data_coa/get_coa4',
            data: {
              // kode_coa_1: rowData.kode_coa_1,
              // kode_coa_2: rowData.kode_coa_2,
              kode_coa_3: rowData.kode_coa,
            },
          },
          columns: [{
              data: "kode_coa"
            },
            {
              data: "description"
            },
          ],
          columnDefs: [{
              targets: [0],
              className: "invoice-date pl-5"
            },
            {
              targets: [1],
              className: "invoice-author px-2"
            },
          ],
          select: false,
        });

        tr.addClass('shown');
      }

    });
    // hapus nasabah
    $('#tabelNasabah tbody').on('click', '.btn-delete', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      swal
        .fire({
          title: "Perhatian",
          html: "Hapus nasabah  <b>[" + datas.no_nasabah + "] " + datas.nama_nasabah + "</b>?",
          // type: "question",
          showCloseButton: true,
          showCancelButton: true,
          confirmButtonColor: "#e74c3c",
          confirmButtonText: "Hapus",
          cancelButtonText: "Tidak"
        })
        .then(function(result) {
          if (result.value) {
            $.ajax({
              url: <?php base_url() ?> 'data_nasabah/delete',
              method: "POST",
              data: {
                id: datas.id
              },
              dataType: "json",
              beforeSend: function() {
                swal.fire({
                  title: 'Please Wait..!',
                  text: 'Is working..',
                  onOpen: function() {
                    swal.showLoading()
                  }
                })
              },
              success: function(data) {
                swal.hideLoading();
                if (data.status == true) {
                  swal.fire({
                    icon: 'success',
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 2000
                  });
                  $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                  swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                }
              }
            })
          } else {}
        });
    });
  }
  $('#form_input_nasabah').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'data_nasabah/add',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btnSave').attr('disabled', true);

      },
      success: function(data) {
        $('#btnSave').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          $('#modal-input').modal('hide');
        } else {
          swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
        };

      }
    });
  });
  $('#form_edit_nasabah').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'data_nasabah/edit',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btnEdit').attr('disabled', true);

      },
      success: function(data) {
        $('#btnEdit').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          $('#modal-edit').modal('hide');
        } else {
          swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
        };

      }
    });
  });
</script>