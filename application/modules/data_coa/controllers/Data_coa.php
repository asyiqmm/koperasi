<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Data_coa extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_coa_model');
		$this->load->model('Auth/Ion_auth_model');
	}

	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view('sidebar', $data);
		$this->load->view('navbar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$data['parent_active'] = 8;
		$data['child_active'] = 16;
		$uri = &load_class('URI', 'core');
		$this->_render('Data_coa_view', $data);
	}

	public function get_coa1(Type $var = null)
	{
		$coa1 = $this->Data_coa_model->get_coa1();
		$obj = array("data" => $coa1);

		echo json_encode($obj);
	}
	
	public function get_coa2(Type $var = null)
	{
		$coa2 = $this->Data_coa_model->get_coa2();
		$obj = array("data" => $coa2);

		echo json_encode($obj);
	}

	public function get_coa3(Type $var = null)
	{
		$coa3 = $this->Data_coa_model->get_coa3();
		$obj = array("data" => $coa3);

		echo json_encode($obj);
	}

	public function get_coa4(Type $var = null)
	{
		$coa4 = $this->Data_coa_model->get_coa4();
		$obj = array("data" => $coa4);

		echo json_encode($obj);
	}

	public function get_selectCoa()
	{

		$coa = $this->Data_coa_model->get_coa();
		$list_coa = [];
		$kode_coa = [];
		$key0 = 0;
		$key1 = 0;
		$key2 = 0;
		$key3 = 0;
		foreach ($coa as $item) {
			array_push($kode_coa, $item->kode_coa);
			if ($item->kode_coa_1 == null) {
				$list_coa[$key0]['kode_coa'] = $item->kode_coa;
				$list_coa[$key0]['kelompok'] = $item->kelompok;
				$list_coa[$key0]['description'] = $item->description;
				$list_coa[$key0]['tipe'] = $item->tipe;
				$key0 = $key0 + 1;
				$key1 = 0;
			} elseif ($item->kode_coa_2 == null) {
				$list_coa[$key1]['kode_coa1'][$item->kode_coa]['kode_coa'] = $item->kode_coa;
				$list_coa[$key1]['kode_coa1'][$item->kode_coa]['kelompok'] = $item->kelompok;
				$list_coa[$key1]['kode_coa1'][$item->kode_coa]['description'] = $item->description;
				$list_coa[$key1]['kode_coa1'][$item->kode_coa]['tipe'] = $item->tipe;
				$key1 = $key1 + 1;
			}
			//  elseif ($item->kode_coa_3 == null) {
			// 	$list_coa[$key2]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['id'] = $item->kode_coa;
			// 	$list_coa[$key2]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['kelompok'] = $item->kelompok;
			// 	$list_coa[$key2]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['description'] = $item->description;
			// 	$list_coa[$key2]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['tipe'] = $item->tipe;
			// 	$key2 = $key2 + 1;
			// } else {
			// 	$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['id'] = $item->kode_coa;
			// 	$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['kelompok'] = $item->kelompok;
			// 	$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['description'] = $item->description;
			// 	$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['tipe'] = $item->tipe;
			// }
		}
		$option = '<option>pilih coa</option>';



		if (isset($_GET['tipe']) == 'json') {
			$result = array('data' => $list_coa);
			echo json_encode($result);die();
		}
	}
}
