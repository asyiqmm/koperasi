<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Data_coa_model extends CI_Model
{
    public function get_coa1(){
        // return $this->db->get('m_coa_1')->result();
        $this->db
        ->where('active',1)
        ->where('kode_coa_1',NULL)
        ->order_by('kode_coa');
        return $this->db->get('m_coa')->result();
    }

    public function get_coa2()
    {
        $this->db
        ->where('kode_coa_1',$_GET['id'])
        ->where('kode_coa_2',NULL)
        ->where('active',1);
        return $this->db->get('m_coa')->result();
    }

    public function get_coa3()
    {
        $this->db->where('kode_coa_2',$_GET['kode_coa_2']);
        $this->db->where('kode_coa_3',NULL);
        $this->db->where('active',1);
        // $this->db->where('kode_coa_1',$_GET['kode_coa_1']);
        return $this->db->get('m_coa')->result();
    }

    public function get_coa4()
    {
        // $this->db->where('kode_coa_1',$_GET['kode_coa_1']);
        // $this->db->where('kode_coa_2',$_GET['kode_coa_2']);
        $this->db->where('kode_coa_3',$_GET['kode_coa_3']);
        $this->db->where('active',1);
        return $this->db->get('m_coa')->result();
    }

    public function get_coa()
    {
        $this->db->where('active',1)
        ->order_by('kode_coa');
        return $this->db->get('m_coa')->result();
    }
}
