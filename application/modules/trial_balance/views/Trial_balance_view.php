<!-- Header -->
<div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Trial Balance</h6>
              <!-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Default</li>
                </ol>
              </nav> -->
            </div>
            <div class="col-lg-6 col-5 text-right">
            <!-- <a a data-toggle="modal" href="#modal-default" class="btn btn-sm btn-neutral"><i class="fas fa-plus"></i> Tambah Data</a> -->
              <!-- <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a> -->
            </div>
          </div>
          <!-- Card stats -->
          
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
    
      <div class="row">
        <div class="col-xl-12">
          <!-- <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Tabel storting</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
             
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Tanggal Transaksi</th>
                    <th scope="col">No Bukti Transaksi</th>
                    <th scope="col">Kode Nasabah</th>
                    <th scope="col">No KTP</th>
                    <th scope="col">Nama Nasabah</th>
                    <th scope="col">NT/RNT/PMT</th>
                    <th scope="col">No Mantri</th>
                    <th scope="col">Nominal</th>
                    <th scope="col">Kas Kecil</th>
                    <th scope="col">Beban Cadangan Piutang Tak Tertagih</th>
                    <th scope="col">Pendapatan Lain Lain</th>
                    <th scope="col">Cadangan Piutang Tak Tertagih</th>
                    <th scope="col">Piutang Usaha</th>
                    <th scope="col">Pendapatan Ditangguhkan - Jasa</th>
                    <th scope="col">Pendapatan Ditangguhkan - Bunga</th>
                    <th scope="col">Pendapatan Jasa</th>
                    <th scope="col">Pendapatan Bunga</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>11/09/2021</td>
                    <td>STR.09.001</td>
                    <td>P1.01-0001</td>
                    <td>3511551203990001</td>
                    <td>Eko Wulandari</td>
                    <td>NT/RNT/PMT</td>
                    <td>01</td>
                    <td>650000</td>
                    <td>if(code=NT){0}elseif(code=RNT){nominal}elseif(code=PMT){0}else{nominal}</td>
                    <td>if(code=NT){nominal}elseif(code=RNT){0}elseif(code=PMT){0}else{0}</td>
                    <td>if(code=RNT){-Cadangan piutang tak tertagih}else{0}</td>
                    <td>if(code=NT){-Nominal}elseif(code=RNT){nominal}elseif(code=PMT){nominal}else{0}</td>
                    <td>if(code=NT){0}elseif(code=RNT){-nominal}else{-nominal}</td>
                    <td>if(code=NT){piutang usaha * 15.384%}elseif(code=RNT){-pendapatan jasa}else{-pendapatan jasa}</td>
                  </tr>
                  
                </tbody>
              </table>
            </div>
          </div> -->

          <div class='card'>
            <div class='card-header'>
              <h4 class='card-title'>Data Trial Balance</h4>
              <div class="card-tools">
              </div>
            </div>

            <div class='card-body table-responsive p-3'>

              <table id='tabelbalance' class='table table-bordered table-hover' style='width:100%'>
                <thead class="bg-primary">
                  <tr>
                    <th rowspan="2">COA</th>
                    <th rowspan="2">Kelompok</th>
                    <th rowspan="2">Deskripsi</th>
                    <th rowspan="2">Jenis</th>
                    <th colspan="2">Saldo Awal</th>
                    <th colspan="2">Transaksi</th>
                    <th colspan="2">Jurnal Umum</th>
                    <th colspan="2">Salso Akhir Neraca</th>
                    <th rowspan="2">Balance</th>
                    <th colspan="2">Laba Rugi</th>
                    <th rowspan="2">P & L</th>
                  </tr>
                  <tr>
                    <th >Debit</th>
                    <th >Kredit</th>
                    <th >Debit</th>
                    <th >Kredit</th>
                    <th >Debit</th>
                    <th >Kredit</th>
                    <th >Debit</th>
                    <th >Kredit</th>
            
                    <th >Debit</th>
                    <th >Kredit</th>
                        
                  </tr>
                </thead>
              </table>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12 col-md-5" id="dataTable_showing">

                </div>
                <div class="col-sm-12 col-md-7" id="dataTable_paginate">

                </div>
              </div>
            </div>
          </div>
        </div>
       
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <!-- <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div>
        </div> -->
      </footer>     
    </div>

   
    <script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script>
  $(function () {
    //Date range picker
    $('#datetimepicker1').datetimepicker({    
        format: 'DD/MM/YYYY',
        maxDate: new Date()
    });
    $('#datetimepicker2').datetimepicker({    
        format: 'DD/MM/YYYY',
        maxDate: new Date()
    });
  })
</script>
<script>
  $(function() {
    $(document).ready(function() {
      list_balance();
    });
  });

  function list_balance() {
    var siteTable = $('#tabelbalance').DataTable({
        "ajax": '<?php echo base_url() ?>trial_balance/tampil_data',
        "scrollY": "600px",
        "scrollCollapse": true,
        "sScrollX": "100%",
        "paging": false,
        // "order": [
        //   [0, "asc"]
        // ],
        "language": {
          "paginate": {
            "next": '<i class="fas fa-angle-right"></i>',
            "previous": '<i class="fas fa-angle-left"></i>',
          }
        },
        initComplete: (settings, json) => {
        $("#dataTable_showing").append($(".dataTables_info"));
        $("#dataTable_paginate").append($(".dataTables_paginate"));
        $(".dataTables_paginate").addClass('float-right')
        },
        "columns": [{
            "data": 'coa',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'kelompok',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'coa_4',
            "className": 'align-middle text-nowrap',
            "width": '15%'
          },
          {
          "defaultContent": 'TRANSAKSI',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          }, 
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          },
          {
          "defaultContent": '-',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          }
         
         
          

        ],
      });

 
  }


</script>
