<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Trial_balance_model extends CI_Model
{
	function tampil_data(){
		//return $this->db->get('t_droping');
		$this->db->select("*,CONCAT(a.kode_coa_1,a.kode_coa_2,a.kode_coa_3, lpad(a.kode_coa_4, 3, '0')) coa")
		->from('m_coa as a');
		$query = $this->db->get();
        // $no_coa_1 = $query->row()->kode_coa_1;
        // $no_coa_2 = $query->row()->kode_coa_2;
        // $no_coa_3 = $query->row()->kode_coa_3;
        // $no_coa_4 = $query->row()->kode_coa_4;
        // $no_coa_4fix = sprintf("%03s",  $no_coa_4);
        // $coa = $no_coa_1.$no_coa_2.$no_coa_3.$no_coa_4fix;
        // $data = array(
		// 	'coa' => $coa,
		// 	'coa_4' =>  $query->row()->coa_4,
		// 	'kelompok' => $query->row()->kelompok
			
		// 	);
        // $no_coa_1 = $query->row()->kode_coa_1;
        return $query->result_array();
	}
}
