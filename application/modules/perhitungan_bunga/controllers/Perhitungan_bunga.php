<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Perhitungan_bunga extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perhitungan_bunga_model');
	}

	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view('sidebar', $data);
		$this->load->view('navbar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		
		$uri = &load_class('URI', 'core');
		$data['user'] = $this->Perhitungan_bunga_model->tampil_data();
		
		$data['nasabah'] = $this->Perhitungan_bunga_model->get_nasabah();
		// $this->load->view('Perhitungan_bunga_view',$data);
		$this->_render('Perhitungan_bunga_view',$data);
	}

	function tambah_aksi(){
		$tgl = $this->input->post('tgl');
		$nama = $this->input->post('nama');
		$no_trans = $this->input->post('no_trans');
		$kode_nasabah = $this->input->post('kode_nasabah');
		$nominal = $this->input->post('nominal');
 
		$data = array(
			'tgl' => date("Y-m-d", strtotime(str_replace('/', '-', $tgl))),
			'no_trans' => $no_trans,
			'kode_nasabah' => $kode_nasabah,
			'nominal' => $nominal
			);
		$this->Perhitungan_bunga_model->input_data($data,'t_bunga');
		redirect('Perhitungan_bunga');
	}

	public function tampil_data(Type $var = null)
	{
		$Perhitungan_bunga = $this->Perhitungan_bunga_model->tampil_data();
		// var_dump($ss);die();
		// $data =  json_decode(json_encode($karyawan->result()), TRUE);
		$obj = array("data" => $Perhitungan_bunga);

		echo json_encode($obj);
	}

	public function edit(Type $var = null)
	{
		$edit = $this->Droping_model->edit_droping();
		
		echo json_encode($edit);
	}

	public function status(Type $var = null)
	{
		$status = $this->Droping_model->status_droping();
		
		echo json_encode($status);
	}

	public function delete(Type $var = null)
	{
		$delete = $this->Droping_model->delete_droping();
		echo json_encode($delete);
	}

	public function add(Type $var = null)
	{
		$add = $this->Droping_model->add_droping();
		echo json_encode($add);
	}
}
