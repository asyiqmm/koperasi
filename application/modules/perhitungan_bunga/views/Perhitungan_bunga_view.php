<!-- Headera -->
<div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Data Perhitungan Bunga</h6>
              <!-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Default</li>
                </ol>
              </nav> -->
            </div>
            <div class="col-lg-6 col-5 text-right">
            <a a data-toggle="modal" href="#modal-input" class="btn btn-sm btn-neutral"><i class="fas fa-plus"></i> Tambah Data</a>
              <!-- <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a> -->
            </div>
          </div>
          <!-- Card stats -->
          
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
    
      <div class="row">
        <div class="col-xl-12">
          

          <div class='card'>
            <div class='card-header'>
              <h4 class='card-title'>Data Perhitungan Bunga</h4>
              <div class="card-tools">
              </div>
            </div>

            <div class='card-body table-responsive p-3'>

              <table id='tabeldroping' class='table table-bordered table-hover' style='width:100%'>
                <thead class="thead-light">
                  <tr>
                    <th >Tanggal</th>
                    <th >Kas Kecil</th>
                    <th >Jasa</th>
                    <th >Bunga</th>
                    <th >Status</th>
                    <th >Opsi</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12 col-md-5" id="dataTable_showing">

                </div>
                <div class="col-sm-12 col-md-7" id="dataTable_paginate">

                </div>
              </div>
            </div>
          </div>
        </div>
       
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <!-- <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div>
        </div> -->
      </footer>
      <div class="modal fade" id="modal-input">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data Bunga</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="" method="post" id="form_input_droping" enctype="multipart/form-data" class="form-horizontal">
              <div class="modal-body">
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' name="tgl" class="form-control" />
                        <span class="input-group-addon input-group-append">

                            <button class="btn btn-outline-primary" type="button" id="button-addon2">  <span class="fa fa-calendar"></span></button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Kas Kecil</label>
                    <input class="form-control" id="kas_kecil" type="number" step="0.01" name="kas_kecil" >
                </div>
                <div class="form-group">
                    <label class="form-control-label">Jasa</label>
                    <input class="form-control" id="jasa" type="number" step="0.01" name="jasa" >
                </div>
                <div class="form-group">
                    <label class="form-control-label">Bunga</label>
                    <input class="form-control" id="bunga" type="number" step="0.01" name="bunga" >
                </div>
                
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btnSave" name="simpan" >Save</button>
              </div>
            </form>  
          </div>
       </div>
      </div>
    </div>

    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Droping</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="" method="post" id="form_edit_droping" enctype="multipart/form-data" class="form-horizontal">
              <div class="modal-body ">
              <div class="form-group">
                  <div class='input-group date' id='datetimepicker2'>
                      <input type='text' name="tgl" class="form-control" id="edit_tgl" />
                      <span class="input-group-addon input-group-append">
                        <button class="btn btn-outline-primary" type="button" id="button-addon2">  <span class="fa fa-calendar"></span></button>
                      </span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="">No Nasabah :</label>
                  <input type="text" readonly class="form-control" id="edit_no_nasabah">
                  <input type="hidden" readonly name="id" id="edit_id">
                </div>
                <div class="form-group">
                  <label for="">Nama Nasabah :</label>
                  <input type="text" readonly class="form-control" name="nama_nasabah" id="edit_nama_nasabah">
                </div>
                <div class="form-group">
                  <label for="">NIK :</label>
                  <input type="number" readonly class="form-control" name="nik" id="edit_nik">
                </div>
                <div class="form-group">
                  <label class="form-control-label">Nominal</label>
                  <input class="form-control" type="number" name="nominal" id="edit_nominal">
                 </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                <button type="Submit" value="Submit" id="btnEdit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

      <div class="modal fade" id="modal-status">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Status Droping</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="" method="post" id="form_status_droping" enctype="multipart/form-data" class="form-horizontal">
              <div class="modal-body ">
              <div class="form-group">
              <label for="">Status</label>
                <div class="row">
                  <div class="col-md-6">
                    <input type="text" readonly class="form-control" id="status_status">
                    <input type="hidden" readonly name="id" id="status_id">
                  </div>
                  <div class="col-md-6">
                    <select class="form-control" name="status">
                        <option value="Approve">Approve</option>
                        <option value="Pending">Pending</option>
                        <option value="Reject">Reject</option>
                    </select>
                  </div>
                    
                    
                </div>
              </div>
                
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                <button type="Submit" value="Submit" id="btnStatus" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    <!-- <script src="assets/vendor/bootstrap-datetimepicker.js"></script> -->
    <script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script>
  $(function () {
    //Date range picker
    $('#datetimepicker1').datetimepicker({    
        format: 'DD/MM/YYYY',
        maxDate: new Date()
    });
    $('#datetimepicker2').datetimepicker({    
        format: 'DD/MM/YYYY',
        maxDate: new Date()
    });
  })
</script>
<script>
  $(function() {
    $(document).ready(function() {
      list_droping();
    });
    function list_droping() {
      var siteTable = $('#tabeldroping').DataTable({
        "ajax": '<?php echo base_url() ?>droping/tampil_data',
        "order": [
          [0, "asc"]
        ],
        "language": {
          "paginate": {
            "next": '<i class="fas fa-angle-right"></i>',
            "previous": '<i class="fas fa-angle-left"></i>',
          }
        },
        initComplete: (settings, json) => {
        $("#dataTable_showing").append($(".dataTables_info"));
        $("#dataTable_paginate").append($(".dataTables_paginate"));
        $(".dataTables_paginate").addClass('float-right')
        },
        "columns": [{
            "data": 'formattgl',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'no_trans',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'kode_nasabah',
            "className": 'align-middle text-nowrap',
            "width": '15%'
          },
          {
            "data": 'nama_nasabah',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'

          },
          {
            "data": 'ktp',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'nominal',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'piutang_usaha',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'kas_kecil',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'jasa',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": 'bunga',
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
            "data": "status",
            render:function(data, type, row)
            {
              if(data == "Pending"){
                return '<button title="status" class="btn btn-outline-warning btn-sm btn-status mr-1">'+data+'</button>';
              }else if(data == "Approve"){
                return '<button title="status" class="btn btn-outline-success btn-sm btn-status mr-1">'+data+'</button>';   
              }else{
                return '<button title="status" class="btn btn-outline-danger btn-sm btn-status mr-1">'+data+'</button>';   
              }
              
            },
            "className": 'align-middle text-nowrap text-center',
            "width": '1%'
          },
          {
          "defaultContent": '<button title="edit" class="btn btn-outline-primary btn-sm btn-edit mr-1">Edit</button><button title="hapus" class="btn btn-outline-danger btn-sm btn-delete">Hapus</button>',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
          }
          

        ],
      });
      $('#tabeldroping tbody').on('click', '.btn-edit', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      $('#modal-edit').modal('show');
      $('#edit_id').val(datas.id);
      $('#edit_no_nasabah').val(datas.kode_nasabah);
      $('#edit_nama_nasabah').val(datas.nama_nasabah);
      $('#edit_nik').val(datas.ktp);
      // $('#edit_status').val(datas.status);s
      $('#edit_nominal').val(datas.nominal);
      $('#edit_tgl').val(datas.formattgl);
    });
    $('#tabeldroping tbody').on('click', '.btn-status', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      $('#modal-status').modal('show');
      $('#status_status').val(datas.status);
      $('#status_id').val(datas.id);
      // $('#edit_no_nasabah').val(datas.kode_nasabah);
      // $('#edit_nama_nasabah').val(datas.nama_nasabah);
      // $('#edit_nik').val(datas.ktp);
      // // $('#edit_status').val(datas.status);s
      // $('#edit_nominal').val(datas.nominal);
      // $('#edit_tgl').val(datas.formattgl);
    });
    $('#tabeldroping tbody').on('click', '.btn-delete', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      swal
        .fire({
          title: "Perhatian",
          html: "Hapus data droping  <b>[" + datas.kode_nasabah + "] " + datas.nama_nasabah + " dengan nominal = " + datas.nominal+"</b>?",
          // type: "question",
          showCloseButton: true,
          showCancelButton: true,
          confirmButtonColor: "#e74c3c",
          confirmButtonText: "Hapus",
          cancelButtonText: "Tidak"
        })
        .then(function(result) {
          if (result.value) {
            $.ajax({
              url: <?php base_url() ?> 'droping/delete',
              method: "POST",
              data: {
                id: datas.id
              },
              dataType: "json",
              beforeSend: function() {
                swal.fire({
                  title: 'Please Wait..!',
                  text: 'Is working..',
                  onOpen: function() {
                    swal.showLoading()
                  }
                })
              },
              success: function(data) {
                swal.hideLoading();
                if (data.status == true) {
                  swal.fire({
                    icon: 'success',
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 2000
                  });
                  $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                  swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                }
              }
            })
          } else {}
        });
    });
    }

    $('#form_edit_droping').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'droping/edit',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btnEdit').attr('disabled', true);

      },
      success: function(data) {
        $('#btnEdit').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          $('#modal-edit').modal('hide');
        } else {
          swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
        };

      }
    });
  });

  $('#form_input_droping').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'droping/add',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btnSave').attr('disabled', true);

      },
      success: function(data) {
        $('#btnSave').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          $('#modal-input').modal('hide');
        } else {
          swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
        };

      }
    });
  });

  $('#form_status_droping').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'droping/status',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btnStatus').attr('disabled', true);

      },
      success: function(data) {
        $('#btnStatus').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          $('#modal-status').modal('hide');
        } else {
          swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
        };

      }
    });
  });
});
 
</script>
<script>
  $('.select_nama').on('change',function(){

        var select = $(this);
        var text = $('option:selected', this).text();
        //console.log(text);
        var ktp = $('option:selected', select).attr('no_ktp');
        document.getElementById('ktp').value = ktp;

        var kode_nasabah = $('option:selected', select).attr('kode_nasabah');
        document.getElementById('kode_nasabah').value = kode_nasabah;
        document.getElementById('kode_nasabah_text').value = kode_nasabah;
        // $('#ktp').val(ktp);
        
        // var un = select.attr('unitovk');
        // var unit = $('option:selected', select).attr('unitovk');
        // $('#'+un).val(unit);
        // $('.'+un).text(unit);
        
        // var no = select.attr('item_no_ovk');
        // $('#'+no).val(select.val());
    })
</script>  