<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Perhitungan_bunga_model extends CI_Model
{ 
    function tampil_data(){
		//return $this->db->get('t_Perhitungan_bunga');
		$this->db->select("*")	
		->from('t_bunga as a')
		->order_by('a.tgl', 'DESC');
		$query = $this->db->get();
        return $query->result_array();
	}
    // ('<button title='status' class='btn btn-outline-primary btn-sm btn-status mr-1'>'+a.status+'</button>') as tombol_status ")	
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	function get_nasabah(){
		$this->db->select('*')
		->from('m_nasabah')
		->order_by('nama_nasabah', 'ASC');
		$query = $this->db->get();
        return $query->result_array();
	}

	public function edit_droping(Type $var = null)
    {
        $this->db->trans_begin();
        $edit_droping = array(
            'tgl' => date("Y-m-d", strtotime(str_replace('/', '-', $_POST['tgl']))),
            'nominal' => $_POST['nominal'],
            'id' => $_POST['id'],
        );
        $this->db->where('id', $_POST['id']);
        $this->db->update('t_droping', $edit_droping);
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil mengubah data droping.'
            ];
        }
    }

    public function status_droping(Type $var = null)
    {
        $this->db->trans_begin();
        $status_droping = array(
            'status' => $_POST['status'],
            'id' => $_POST['id'],
        );
        $this->db->where('id', $_POST['id']);
        $this->db->update('t_droping', $status_droping);
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil mengubah data droping.'
            ];
        }
    }

	public function delete_droping(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->set('active', 0);
        $this->db->where('id', $_POST['id']);
        $this->db->update('t_droping');
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menghapus data droping.'
            ];
        }
    }

	public function add_droping(Type $var = null)
    {
        $kode_bulan = substr($_POST['tgl'], 3, 2);
        $this->db->trans_begin();
        $this->db->select('MAX(running_number + 1) as running_number')
		->where('kode_bulan', $kode_bulan)
        ->from('t_droping');
        $running_number = $this->db->get();
        $number = $running_number->row()->running_number;
		$kode_number = sprintf("%05s", $number);
		$no_trans = "DRP.".$kode_bulan.".".$kode_number;
		$data = array(
			'tgl' => date("Y-m-d", strtotime(str_replace('/', '-', $_POST['tgl']))),
			'no_trans' => $_POST['no_trans'],
			'kode_nasabah' => $_POST['kode_nasabah'],
			'nominal' => $_POST['nominal'],
            'running_number' => $number,
			'kode_bulan' => $kode_bulan,
			'no_trans' => $no_trans
            
			);
        $this->db->insert('t_droping',$data);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambah data droping.'
            ];
        }
    }
}
