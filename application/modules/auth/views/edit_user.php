<!--
=========================================================
* Argon Dashboard - v1.2.0
=========================================================
* Product Page: https://www.creative-tim.com/product/argon-dashboard

* Copyright  Creative Tim (http://www.creative-tim.com)
* Coded by www.creative-tim.com
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html>

<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
      <meta name="author" content="Creative Tim">
      <title>Edit User - Koperasi</title>
      <!-- Favicon -->
      <link rel="icon" href="<?php echo base_url() ?>assets/img/brand/favicon.png" type="image/png">
      <!-- Fonts -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
      <!-- Icons -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/nucleo/css/nucleo.css" type="text/css">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
      <!-- Select2 -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/select2/dist/css/select2.min.css">
      <!-- Argon CSS -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body class="bg-default">
      <!-- Navbar -->
      <nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
            <div class="container">
                  <a class="navbar-brand" href="dashboard.html">
                        <img src="<?php echo base_url() ?>assets/img/brand/white.png">
                  </a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
                        <div class="navbar-collapse-header">
                              <div class="row">
                                    <div class="col-6 collapse-brand">
                                          <a href="<?php echo base_url() ?>">
                                                <img src="<?php echo base_url() ?>assets/img/brand/blue.png">
                                          </a>
                                    </div>
                                    <div class="col-6 collapse-close">
                                          <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                                                <span></span>
                                                <span></span>
                                          </button>
                                    </div>
                              </div>
                        </div>

                  </div>
            </div>
      </nav>
      <!-- Main content -->
      <div class="main-content">
            <!-- Header -->
            <div class="header bg-gradient-primary py-7 py-lg-6 pt-lg-5">
                  <div class="container">
                        <div class="header-body text-center mb-7">
                              <div class="row justify-content-center">
                                    <div class="col-xl-5 col-lg-6 col-md-8 pm-5">
                                          <h1 class="text-white">Edit Data User</h1>
                                    </div>
                              </div>
                        </div>
                  </div>
                  <div class="separator separator-bottom separator-skew zindex-100">
                        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                              <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                        </svg>
                  </div>
            </div>
            <!-- Page content -->
            <div class="container mt--8 pb-5">
                  <!-- Table -->
                  <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-8">
                              <div class="card bg-secondary border-0">
                                    <div class="card-body px-lg-5 py-lg-5">
                                          <?php echo form_open(uri_string()); ?>
                                          <?php echo form_hidden('id', $user->id); ?>
                                          <?php echo form_hidden($csrf); ?>
                                          <div class="row">
                                                <div class="col-md-7">
                                                      <div class="form-group">
                                                            <label for="nama_karyawan">Nama Karyawan:</label>
                                                            <?php echo form_input($nama_karyawan); ?>
                                                      </div>
                                                </div>
                                                <div class="col-md-5">
                                                      <div class="form-group">
                                                            <label for="username">Username:</label>
                                                            <?php echo form_input($identity); ?>

                                                      </div>
                                                </div>
                                          </div>
                                          <div class="row">
                                                <div class="col-md-6">
                                                      <div class="form-group">
                                                            <label for="nik">No KTP:</label>
                                                            <?php echo form_input($nik); ?>
                                                      </div>
                                                </div>
                                                <div class="col-md-6">
                                                      <div class="form-group">
                                                            <label for="kk">No KK:</label>
                                                            <?php echo form_input($kk); ?>

                                                      </div>
                                                </div>
                                          </div>

                                          <div class="form-group">
                                                <label for="alamat_ktp">Alamat KTP:</label>
                                                <textarea name="alamat_ktp" id="alamat_ktp" class="form-control" cols="1" rows="2"><?php echo $alamat_ktp; ?></textarea>
                                          </div>
                                          <div class="form-group">
                                                <label for="alamat_tinggal">Alamat Tinggal:</label>
                                                <textarea name="alamat_tinggal" id="alamat_tinggal" class="form-control" cols="1" rows="2"><?php echo $alamat_tinggal; ?></textarea>
                                          </div>
                                          <div class="form-group">
                                                <?php echo lang('edit_user_password_label', 'password'); ?>
                                                <!-- <input type="password" name="password" class="form-control" id="password" placeholder="Enter password"> -->
                                                <?php echo form_input($password); ?>

                                          </div>
                                          <div class="form-group">
                                                <?php echo lang('edit_user_password_confirm_label', 'password_confirm'); ?>
                                                <!-- <input type="password" name="password_confirm" class="form-control" id="password_confirm" placeholder="Confirm password"> -->
                                                <?php echo form_input($password_confirm); ?>
                                          </div>
                                          <?php if (!$this->ion_auth->is_admin()) : ?>
                                                <div class="form-group">
                                                      <label for="">Koperasi</label>
                                                      <select class="form-control" required placeholder="Pilih Mantri" name="kode_pos" id="kode_pos" data-toggle="select" title="Simple select" data-live-search="true" data-live-search-placeholder="Search ...">
                                                            <option value=""></option>
                                                            <?php foreach ($list_koperasi as $kp) {
                                                                  if ($kp->id == $kode_pos) {
                                                                        $selected = 'selected';
                                                                  } else {
                                                                        $selected = '';
                                                                  }
                                                                  echo '<option ' . $selected . ' value="' . $kp->id . '">' . $kp->nm_pos . '</option>';
                                                            } ?>
                                                      </select>
                                                </div>
                                                <div class="form-group">
                                                      <!--<h3><?php echo lang('edit_user_groups_heading'); ?></h3>-->
                                                      <!--<label for="username">Level user:</label>-->
                                                      <?php foreach ($groups as $group) : ?>
                                                            <label class="radio">
                                                                  <?php
                                                                  $gID = $group['id'];
                                                                  $checked = null;
                                                                  $item = null;
                                                                  foreach ($currentGroups as $grp) {
                                                                        if ($gID == $grp->id) {
                                                                              $checked = ' checked="checked"';
                                                                              break;
                                                                        }
                                                                  }
                                                                  ?>
                                                                  <input type="radio" class="" name="groups[]" value="<?php echo $group['id']; ?>" <?php echo $checked; ?>>
                                                                  <?php echo htmlspecialchars($group['description'], ENT_QUOTES, 'UTF-8'); ?>
                                                            </label>
                                                      <?php endforeach ?>
                                                </div>
                                          <?php endif ?>
                                          
                                          <div class="row">
                                                <div class="col-4">
                                                      <button type="button" class="btn btn-secondary btn-block" onclick="goBack()">Back</button>

                                                </div>
                                                <div class="col-4">

                                                </div>
                                                <!-- /.col -->
                                                <div class="col-4">
                                                      <button type="submit" class="btn btn-primary btn-block">Save</button>
                                                </div>
                                                <!-- /.col -->
                                          </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
      <!-- Footer -->
      <footer class="py-5" id="footer-main">
            <div class="container">
                  <div class="row align-items-center justify-content-xl-between">
                        <div class="col-xl-6">
                              <div class="copyright text-center text-xl-left text-muted">
                                    &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
                              </div>
                        </div>
                        <div class="col-xl-6">
                              <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                                    <li class="nav-item">
                                          <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
                                    </li>
                              </ul>
                        </div>
                  </div>
            </div>
      </footer>
      <!-- Argon Scripts -->
      <!-- Core -->
      <script src="<?php echo base_url() ?>assets/vendor/jquery/dist/jquery.min.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/js-cookie/js.cookie.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
      <!-- Argon JS -->
      <script src="<?php echo base_url() ?>assets/js/argon.js?v=1.2.0"></script>
      <!-- Select2 -->
      <script src="<?php echo base_url() ?>assets/vendor/select2/dist/js/select2.min.js"></script>
      <script>
            $(document).ready(function() {
                  $('#mantri').select2({
                        placeholder: "Pilih mantri",
                        allowClear: true
                  });
                  $('#kode_pos').select2({
                        placeholder: "Pilih kode pos",
                        allowClear: true
                  });
            })
      </script>
</body>

</html>