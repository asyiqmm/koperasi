<!--
=========================================================
* Argon Dashboard - v1.2.0
=========================================================
* Product Page: https://www.creative-tim.com/product/argon-dashboard

* Copyright  Creative Tim (http://www.creative-tim.com)
* Coded by www.creative-tim.com
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html>

<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
      <meta name="author" content="Creative Tim">
      <title>Argon Dashboard - Free Dashboard for Bootstrap 4</title>
      <!-- Favicon -->
      <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
      <!-- Fonts -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
      <!-- Icons -->
      <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
      <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
      <!-- Select2 -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/select2/dist/css/select2.min.css">
      <!-- Argon CSS -->
      <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body class="bg-default">
      <!-- Navbar -->
      <nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
            <div class="container">
                  <a class="navbar-brand" href="dashboard.html">
                        <img src="../assets/img/brand/white.png">
                  </a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
                        <div class="navbar-collapse-header">
                              <div class="row">
                                    <div class="col-6 collapse-brand">
                                          <a href="<?php echo base_url() ?>">
                                                <img src="../assets/img/brand/blue.png">
                                          </a>
                                    </div>
                                    <div class="col-6 collapse-close">
                                          <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                                                <span></span>
                                                <span></span>
                                          </button>
                                    </div>
                              </div>
                        </div>

                  </div>
            </div>
      </nav>
      <!-- Main content -->
      <div class="main-content">
            <!-- Header -->
            <div class="header bg-gradient-primary py-7 py-lg-6 pt-lg-5">
                  <div class="container">
                        <div class="header-body text-center mb-7">
                              <div class="row justify-content-center">
                                    <div class="col-xl-5 col-lg-6 col-md-8 pm-5">
                                          <h1 class="text-white">Buat Karyawan Baru</h1>

                                    </div>
                              </div>
                        </div>
                  </div>
                  <div class="separator separator-bottom separator-skew zindex-100">
                        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                              <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                        </svg>
                  </div>
            </div>
            <!-- Page content -->
            <div class="container mt--8 pb-5">
                  <!-- Table -->
                  <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-8">
                              <div class="card bg-secondary border-0">
                                    <div class="card-body px-lg-5 py-lg-5">
                                          <form role="form" action="<?php echo base_url() ?>auth/create_user" method="post">
                                          <?php echo form_hidden($csrf); ?>
                                                <div class="row">
                                                      <div class="col-md-7">
                                                            <div class="form-group">
                                                                  <div class="input-group input-group-merge input-group-alternative mb-3">
                                                                        <div class="input-group-prepend">
                                                                              <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                                                        </div>
                                                                        <?php echo form_input($nama_karyawan); ?>
                                                                  </div>
                                                            </div>
                                                      </div>
                                                      <div class="col-md-5">
                                                            <div class="form-group">
                                                                  <div class="input-group input-group-merge input-group-alternative mb-3">
                                                                        <div class="input-group-prepend">
                                                                              <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                                                        </div>
                                                                        <?php echo form_input($identity); ?>
                                                                  </div>
                                                            </div>

                                                      </div>
                                                </div>


                                                <div class="row">
                                                      <div class="col-md-6">
                                                            <div class="form-group">
                                                                  <div class="input-group input-group-merge input-group-alternative mb-3">
                                                                        <div class="input-group-prepend">
                                                                              <span class="input-group-text"><i class="ni ni-credit-card"></i></span>
                                                                        </div>
                                                                        <?php echo form_input($nik); ?>

                                                                  </div>
                                                            </div>
                                                      </div>

                                                      <div class="col-md-6">
                                                            <div class="form-group">
                                                                  <div class="input-group input-group-merge input-group-alternative mb-3">
                                                                        <div class="input-group-prepend">
                                                                              <span class="input-group-text"><i class="ni ni-paper-diploma"></i></span>
                                                                        </div>
                                                                        <?php echo form_input($kk); ?>
                                                                  </div>
                                                            </div>
                                                      </div>
                                                </div>

                                                <div class="form-group">
                                                      <div class="input-group input-group-merge input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                  <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                                            </div>
                                                            <textarea class="form-control" required name="alamat_ktp" placeholder="Alamat KTP" type="text"></textarea>
                                                      </div>
                                                </div>
                                                <div class="form-group">
                                                      <div class="input-group input-group-merge input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                  <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                                            </div>
                                                            <textarea class="form-control" required name="alamat_tinggal" placeholder="Alamat Tempat Tinggal" type="text"></textarea>
                                                      </div>
                                                </div>
                                                <div class="form-group">
                                                      <select class="form-control" required placeholder="Pilih Mantri" name="kode_pos" id="kode_pos" data-toggle="select" title="Simple select" data-live-search="true" data-live-search-placeholder="Search ...">
                                                            <option value=""></option>
                                                            <?php foreach ($kode_pos as $kp) {
                                                                  echo '<option value="' . $kp->id . '">' . $kp->nm_pos . '</option>';
                                                            } ?>
                                                      </select>
                                                </div>
                                                <!-- <div class="form-group">
                                                      <select class="form-control" required placeholder="Pilih Mantri" name="mantri" id="mantri" data-toggle="select" title="Simple select" data-live-search="true" data-live-search-placeholder="Search ...">
                                                            <option value=""></option>
                                                            <?php foreach ($mantri as $m) {
                                                                  echo '<option value="' . $m->id . '">' . $m->nama . '</option>';
                                                            } ?>
                                                      </select>
                                                </div> -->
                                                <div class="form-group">
                                                      <div class="input-group input-group-merge input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                            </div>
                                                            <input type="password" name="password" class="form-control" id="password" placeholder="enter password">
                                                      </div>
                                                </div>
                                                <div class="form-group">
                                                      <div class="input-group input-group-merge input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                            </div>
                                                            <?php echo form_input($password_confirm); ?>
                                                      </div>
                                                </div>
                                                <?= form_dropdown('groups[]', $listGroups, '1', 'id = "groups" required') ?>

                                                <div class="text-center">
                                                      <div id="infoMessage"><?php echo $message; ?></div>
                                                      <button type="submit" class="btn btn-primary mt-4">Buat Akun</button>
                                                </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
      <!-- Footer -->
      <footer class="py-5" id="footer-main">
            <div class="container">
                  <div class="row align-items-center justify-content-xl-between">
                        <div class="col-xl-6">
                              <div class="copyright text-center text-xl-left text-muted">
                                    &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
                              </div>
                        </div>
                        <div class="col-xl-6">
                              <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                                    <li class="nav-item">
                                          <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
                                    </li>
                                    <li class="nav-item">
                                          <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
                                    </li>
                              </ul>
                        </div>
                  </div>
            </div>
      </footer>
      <!-- Argon Scripts -->
      <!-- Core -->
      <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
      <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
      <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
      <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
      <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
      <!-- Argon JS -->
      <script src="../assets/js/argon.js?v=1.2.0"></script>
      <!-- Select2 -->
      <script src="<?php echo base_url() ?>assets/vendor/select2/dist/js/select2.min.js"></script>
      <script>
            $(document).ready(function() {
                  $('#mantri').select2({
                        placeholder: "Pilih mantri",
                        allowClear: true
                  });
                  $('#groups').select2({
                        placeholder: "Pilih Role",
                        allowClear: true
                  }).val('').trigger('change');
                  $('#kode_pos').select2({
                        placeholder: "Pilih kode pos",
                        allowClear: true
                  });
            })
      </script>
</body>

</html>