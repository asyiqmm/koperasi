<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Data_dimension_model extends CI_Model
{
    public function get_pos()
    {
        $this->db->select('*')
            ->from('m_kode_pos')
            ->where('active', 1)
            ->order_by('kode_pos');
        $result = $this->db->get();
        return $result;
    }

    public function get_dimension()
    {
        $this->db->select('*')
            ->from('d_dimension a')
            ->join('m_kode_pos b', 'b.kode_pos = a.kode_pos_id')
            ->join('m_mantri c', 'c.id = a.mantri_id')
            ->where('a.active', 1)
            ->where('b.number', $_GET['id'])
            ->order_by('a.kode_pos_id,a.mantri_id');
        $result = $this->db->get();
        // var_dump($this->db->last_query());die();
        return $result;
    }

    public function add_dimension($data)
    {
        $this->db->trans_begin();
        $this->db->insert('d_dimension', $data);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambah mantri pada pos.'
            ];
        }
    }

    public function get_existMantri()
    {
        'SELECT *
        FROM m_kode_pos a
        LEFT JOIN d_dimension b ON b.kode_pos_id = a.kode_pos
        RIGHT JOIN m_mantri c ON c.kode = b.mantri_id and a.number = 2
        WHERE c.active = 1
        and a.number IS NULL';

        $this->db->select('c.kode,c.nama')
            ->from('m_mantri c')
            ->where('c.active', 1)
            ->order_by('c.kode', 'ASC');
        $result = $this->db->get();
        // var_dump($this->db->last_query());die();
        return $result;
    }

    public function add_pos()
    {
        $this->db->trans_begin();
        $this->db->select_max('number');
        $number = $this->db->get('m_kode_pos')->row();
        $_POST['number'] = $number->number + 1;
        $this->db->insert('m_kode_pos', $_POST);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambah pos baru.'
            ];
        }
    }
}
