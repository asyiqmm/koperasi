<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Data Dimension</h6>

        </div>
        <div class="col-lg-6 col-5 text-right">
          <button class="btn btn-sm btn-neutral" data-target="#modal-add-pos" data-toggle="modal">Tambah Dimension</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">

  <div class='card'>
    <div class='card-header'>
      <h4 class='card-title'>Data Dimension</h4>
      <div class="card-tools">
      </div>
    </div>

    <div class='card-body table-responsive p-3'>

      <table id='tabelDimension' class='table table-bordered table-hover' width="100%">
        <thead class="thead-light">
          <tr>
            <th width="1%">KODE</th>
            <th>NAMA POS</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-sm-12 col-md-5" id="dataTable_showing">

        </div>
        <div class="col-sm-12 col-md-7" id="dataTable_paginate">

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>
<!-- Modal -->
<div class="modal fade" id="mantriModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Daftar Mantri</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <table id="tableMantri" class="table table-bordered" width="100%">
          <thead>
            <th>Kode Mantri</th>
            <th>Nama Mantri</th>
            <th></th>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- form mantri -->
<div class="modal fade" id="modal-add-mantri">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Mantri</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <form action="" id="form-mantri-pos">
          <input type="hidden" name="idPos" id="idPos">
          <div class="form-group">
            <label for="selectMantri">Mantri</label>
            <select class="form-control form-control-sm" name="mantri" id="selectMantri">
            </select>
          </div>
          <button class="btn btn-sm btn-primary btn-save" type="submit">Tambah</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- form dimension -->
<div class="modal fade" id="modal-add-pos">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Dimension/Pos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <form action="" id="form-dimension-pos">
          <div class="form-group">
            <label for="selectMantri">Kode Pos</label>
            <input class="form-control form-control-sm" name="kode_pos" placeholder="example:P1">
          </div>
          <div class="form-group">
            <label for="selectMantri">Nama Pos</label>
            <input class="form-control form-control-sm" name="nm_pos" placeholder="example:Surabaya">
          </div>
          <button class="btn btn-sm btn-primary btn-save" type="submit">Tambah</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- JS -->
<script>
  $(function() {
    $(document).ready(function() {
      list_dimension();
      // $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
    });
  });
  $('#modal-add-mantri').on('hidden.bs.modal', function() {
    // Load up a new modal...
    $('#mantriModal').modal('show');
  });

  $('#form-mantri-pos').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'data_master_dimension/add_dimension',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btn-save').attr('disabled', true);

      },
      success: function(data) {
        $('#btn-save').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });

          $('#modal-add-mantri').modal('hide');
        } else {

          swal.fire("!Opps ", 'Mantri sudah ada di pos yang dipilih.', "error");
        };

      }
    });
  });

  $('#form-dimension-pos').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'data_master_dimension/add_pos',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btn-save').attr('disabled', true);

      },
      success: function(data) {
        $('#btn-save').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });

          $('#modal-add-pos').modal('hide');
        } else {

          swal.fire("!Opps ", 'Gagal menambah pos baru.', "error");
        };

      }
    });
  });

  function tableMantri(id, kd_pos) {
    $('#idPos').val(kd_pos);
    $.ajax({
      url: <?php base_url() ?> 'data_master_dimension/get_existMantri?id=' + id,
      type: "get",
      dataType: "JSON",
      beforeSend: function() {
        $('#selectMantri').find('option').remove();
      },
      success: function(data) {
        var data = data.data;
        var options = '<option disabled selected>~~~Pilih Mantri~~</option>';
        for (var i = 0; i < data.length; i++) {
          options += '<option value="' + data[i].kode + '">' + data[i].nama + '</option>';
        }

        $('#selectMantri').append(options);
      }
    });
    var siteTable = $('#tableMantri').DataTable({
      "ajax": '<?php echo base_url() ?>data_master_dimension/get_dimension?id=' + id,
      "info": false,
      "destroy": true,
      "order": [
        [0, "asc"]
      ],
      paging: false,
      buttons: [{
        text: 'Tambah Mantri',
        className: 'btn btn-primary btn-sm btn-add',
        action: function(e, dt, node, config) {
          console.log(dt)
          $('#mantriModal').modal('hide');
          $('.btn-add')
            .attr('data-toggle', 'modal')
            .attr('data-target', '#modal-add-mantri');
        }
      }, ],
      "columns": [{
          "data": 'mantri_id',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'nm_mantri',
          "className": 'align-middle text-nowrap text-left',
          "width": '95%'
        },
        {
          "className": 'align-middle text-nowrap text-center',
          "width": '1%',
          "render": function(data, type, row, meta) {
            var a = '';
            a += '<a class="btn btn-outline-danger btn-sm btn-hapus" href="javascript:void(0)" title="hapus user">Hapus</a>';
            return a;
          }
        },

      ],
      "columnDefs": [{
        "targets": '_all',
        "defaultContent": '0'
      }],
      iDisplayLength: 100,
      initComplete: function() {
        this.api().buttons().container()
          .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
      }
    });
  }

  function list_dimension() {
    var siteTable = $('#tabelDimension').DataTable({
      "ajax": '<?php echo base_url() ?>data_master_dimension/get_pos',
      "order": [
        [0, "asc"]
      ],
      "language": {
        "paginate": {
          "next": '<i class="fas fa-angle-right"></i>',
          "previous": '<i class="fas fa-angle-left"></i>',
        }
      },
      initComplete: (settings, json) => {
        $("#dataTable_showing").append($(".dataTables_info"));
        $("#dataTable_paginate").append($(".dataTables_paginate"));
        $(".dataTables_paginate").addClass('float-right')
      },
      "columns": [{
          "data": 'number',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'nm_pos',
          "className": 'align-middle text-nowrap text-left',
          "width": '25%'
        },
        {
          "className": 'align-middle text-nowrap text-center',
          "width": '1%',
          "render": function(data, type, row, meta) {
            var a = '<button type="button" class="btn btn-outline-success btn-sm" title="lihat mantri" onClick=tableMantri("' + row.number + '","' + row.kode_pos + '") data-toggle="modal" data-target="#mantriModal">Lihat Mantri</button>\n';
            a += '<a class="btn btn-outline-warning btn-sm" href="" title="edit user">Edit</a>\n';
            a += '<a class="btn btn-outline-danger btn-sm btn-hapus" href="javascript:void(0)" title="hapus user">Hapus</a>';
            return a;
          }
        },

      ],
    });
    siteTable.on('order.dt search.dt', function() {
      siteTable.column(0, {
        search: 'applied',
        order: 'applied'
      }).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();
    $('#tabelKaryawan tbody').on('click', '.btn-active', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      if (<?php echo $this->ion_auth->get_users_groups()->row()->id ?> < datas.id_role) {
        swal
          .fire({
            title: "Perhatian",
            html: "Nonaktifkan User  <b>[" + datas.no_karyawan + "] " + datas.nama_karyawan + "</b>?",
            // type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#e74c3c",
            confirmButtonText: "Hapus",
            cancelButtonText: "Tidak"
          })
          .then(function(result) {
            if (result.value) {
              $.ajax({
                url: <?php base_url() ?> 'auth/deactivate/' + datas.id,
                method: "POST",
                data: {
                  confirm: 'yes',
                  id: datas.id,
                },
                dataType: "json",
                beforeSend: function() {
                  swal.fire({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    onOpen: function() {
                      swal.showLoading()
                    }
                  })
                },
                success: function(data) {
                  swal.hideLoading();
                  if (data.status == true) {
                    swal.fire({
                      icon: 'success',
                      title: data.msg,
                      showConfirmButton: false,
                      timer: 2000
                    });
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                  } else {
                    swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                  }
                }
              })
            } else {}
          });
      } else {
        swal.fire("!Opps ", "Anda tidak mempunyai akses untuk menonaktifkan user ini", "error");
      }

    });



    $('#tabelKaryawan tbody').on('click', '.btn-hapus', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      swal
        .fire({
          title: "Perhatian",
          html: "Hapus data pengguna  <b>[" + datas.no_karyawan + "] " + datas.nama_karyawan + "</b>?",
          // type: "question",
          showCloseButton: true,
          showCancelButton: true,
          confirmButtonColor: "#e74c3c",
          confirmButtonText: "Hapus",
          cancelButtonText: "Tidak"
        })
        .then(function(result) {
          if (result.value) {
            $.ajax({
              url: <?php base_url() ?> 'data_karyawan/delete',
              method: "POST",
              data: {
                id: datas.id
              },
              dataType: "json",
              beforeSend: function() {
                swal.fire({
                  title: 'Please Wait..!',
                  text: 'Is working..',
                  onOpen: function() {
                    swal.showLoading()
                  }
                })
              },
              success: function(data) {
                swal.hideLoading();
                if (data.status == true) {
                  swal.fire({
                    icon: 'success',
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 2000
                  });
                  $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                  swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                }
              }
            })
          } else {}
        });
    });
  }
</script>