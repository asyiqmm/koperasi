<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Data_master_dimension extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_dimension_model');
	}

	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view('sidebar', $data);
		$this->load->view('navbar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$data['parent_active'] = 8;
		$data['child_active'] = 17;
		$uri = &load_class('URI', 'core');
		$this->_render('Data_dimension_view', $data);
	}


	public function get_pos()
	{
		$pos = $this->Data_dimension_model->get_pos();
		$obj = array("data" => $pos->result());

		echo json_encode($obj);
	}

	public function get_dimension()
	{
		$dimension = $this->Data_dimension_model->get_dimension();
		$result = array();
		foreach ($dimension->result() as $key => $value) {
			$result[] = array(
				'nm_mantri'		=> $value->nama,
				'nm_pos'		=> $value->number . ' : ' . $value->nm_pos,
				'pos_id'		=> $value->kode_pos_id,
				'mantri_id'		=> $value->mantri_id,
			);
		}
		$obj = array("data" => $result);

		echo json_encode($obj);
	}

	public function get_existMantri()
	{
		$pos = $this->Data_dimension_model->get_existMantri();
		$obj = array("data" => $pos->result());

		echo json_encode($obj);
	}

	public function add_dimension()
	{
		$add = array(
			'kode_pos_id' 	=> $_POST['idPos'],
			'mantri_id'		=> $_POST['mantri']);
		$post = $this->Data_dimension_model->add_dimension($add);
		echo json_encode($post);
	}

	public function add_pos()
	{
		$post = $this->Data_dimension_model->add_pos();
		echo json_encode($post);
	}
}
