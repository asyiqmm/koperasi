<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Data_karyawan_model extends CI_Model
{
	public function get_karyawan()
    {
        $query = 
        $this->db->select("a.active ,no_karyawan, CONCAT(a.tipe_karyawan, '.', `b`.`nm_pos`, '-', lpad(a.mantri_id, 2, '0'), '.',lpad(a.running_number, 5, '0')) no_user, `a`.`nama_karyawan`, `a`.`nik`, `a`.`kk`, `a`.`alamat_ktp`, `a`.`alamat_tinggal` ,a.id ,d.description role, d.id id_role")
        ->from('users a')
        ->join('m_kode_pos b','b.id = a.kode_pos','left')
        ->join('users_groups c','c.user_id = a.id')
        ->join('groups d','d.id = c.group_id')
        ->get();
        // var_dump($this->db->last_query());die();
        return $query->result_array();
    }
}
