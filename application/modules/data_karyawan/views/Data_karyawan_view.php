<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Data Karyawan</h6>

        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="<?php echo base_url() ?>auth/create_user" class="btn btn-sm btn-neutral">Tambah Karyawan</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">

  <div class='card'>
    <div class='card-header'>
      <h4 class='card-title'>Data Karyawan</h4>
      <div class="card-tools">
      </div>
    </div>

    <div class='card-body table-responsive p-3'>

      <table id='tabelKaryawan' class='table table-bordered table-hover' width="100%">
        <thead class="thead-light">
          <tr>
            <th>No</th>
            <th>Nama Karyawan</th>
            <th>NIK</th>
            <th>NO KK</th>
            <th>ALAMAT KTP</th>
            <th>ALAMAT TEMPAT TINGGAL</th>
            <th>ROLE</th>
            <th>STATUS</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-sm-12 col-md-5" id="dataTable_showing">

        </div>
        <div class="col-sm-12 col-md-7" id="dataTable_paginate">

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>
<!-- JS -->
<script>
  $(function() {
    $(document).ready(function() {
      list_karyawan();
      // $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
    });
  });

  function list_karyawan() {
    var siteTable = $('#tabelKaryawan').DataTable({
      "ajax": '<?php echo base_url() ?>data_karyawan/get_karyawan',
      "order": [
        [0, "asc"]
      ],
      "language": {
        "paginate": {
          "next": '<i class="fas fa-angle-right"></i>',
          "previous": '<i class="fas fa-angle-left"></i>',
        }
      },
      initComplete: (settings, json) => {
        $("#dataTable_showing").append($(".dataTables_info"));
        $("#dataTable_paginate").append($(".dataTables_paginate"));
        $(".dataTables_paginate").addClass('float-right')
      },
      "columns": [{
          "data": 'no_karyawan',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'nama_karyawan',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'nik',
          "className": 'align-middle text-nowrap',
          "width": '1%'
        },
        {
          "data": 'kk',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'alamat_ktp',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'alamat_tinggal',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": "role",
          "className": 'align-middle text-nowrap text-center',
          "width": '1%',
        },
        {
          "className": 'align-middle text-nowrap text-center',
          "width": '1%',
          "render": function(data, type, row, meta) {
            if (row.active == 1) {
              var a = '<a href="javascript:void(0)" class="btn btn-outline-success btn-sm btn-active" title="deactive user">Active</a>';
            } else {
              var a = '<a class="btn btn-outline-success btn-sm" href="<?php echo base_url() . "auth/edit_user/" ?>' + row.id + '" title="edit user">Deactive</a>\n';
            }

            return a;
          }
        },
        {
          "className": 'align-middle text-nowrap text-center',
          "width": '1%',
          "render": function(data, type, row, meta) {
            var a = '<a class="btn btn-outline-warning btn-sm" href="<?php echo base_url() . "auth/edit_user/" ?>' + row.id + '" title="edit user">Edit</a>\n';
            a += '<a class="btn btn-outline-danger btn-sm btn-hapus" href="javascript:void(0)" title="hapus user">Hapus</a>';
            return a;
          }
        },

      ],
    });
    $('#tabelKaryawan tbody').on('click', '.btn-active', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      if (<?php echo $this->ion_auth->get_users_groups()->row()->id ?> < datas.id_role) {
        swal
          .fire({
            title: "Perhatian",
            html: "Nonaktifkan User  <b>[" + datas.no_karyawan + "] " + datas.nama_karyawan + "</b>?",
            // type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#e74c3c",
            confirmButtonText: "Hapus",
            cancelButtonText: "Tidak"
          })
          .then(function(result) {
            if (result.value) {
              $.ajax({
                url: <?php base_url() ?> 'auth/deactivate/'+datas.id,
                method: "POST",
                data: {
                  confirm: 'yes',
                  id: datas.id,
                },
                dataType: "json",
                beforeSend: function() {
                  swal.fire({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    onOpen: function() {
                      swal.showLoading()
                    }
                  })
                },
                success: function(data) {
                  swal.hideLoading();
                  if (data.status == true) {
                    swal.fire({
                      icon: 'success',
                      title: data.msg,
                      showConfirmButton: false,
                      timer: 2000
                    });
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                  } else {
                    swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                  }
                }
              })
            } else {}
          });
      } else {
        swal.fire("!Opps ", "Anda tidak mempunyai akses untuk menonaktifkan user ini", "error");
      }

    });
    $('#tabelKaryawan tbody').on('click', '.btn-hapus', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      swal
        .fire({
          title: "Perhatian",
          html: "Hapus data pengguna  <b>[" + datas.no_karyawan + "] " + datas.nama_karyawan + "</b>?",
          // type: "question",
          showCloseButton: true,
          showCancelButton: true,
          confirmButtonColor: "#e74c3c",
          confirmButtonText: "Hapus",
          cancelButtonText: "Tidak"
        })
        .then(function(result) {
          if (result.value) {
            $.ajax({
              url: <?php base_url() ?> 'data_karyawan/delete',
              method: "POST",
              data: {
                id: datas.id
              },
              dataType: "json",
              beforeSend: function() {
                swal.fire({
                  title: 'Please Wait..!',
                  text: 'Is working..',
                  onOpen: function() {
                    swal.showLoading()
                  }
                })
              },
              success: function(data) {
                swal.hideLoading();
                if (data.status == true) {
                  swal.fire({
                    icon: 'success',
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 2000
                  });
                  $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                  swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                }
              }
            })
          } else {}
        });
    });
  }
</script>