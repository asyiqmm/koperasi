<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Data_karyawan extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_karyawan_model');
	}

	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view('sidebar', $data);
		$this->load->view('navbar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$data['parent_active'] = 8;
		$data['child_active'] = 14;
		$uri = &load_class('URI', 'core');
		$this->_render('Data_karyawan_view',$data);
	}

	public function get_karyawan(Type $var = null)
	{
		$karyawan = $this->Data_karyawan_model->get_karyawan();
		// var_dump($karyawan);die();
		// $data =  json_decode(json_encode($karyawan->result()), TRUE);
		$obj = array("data" => $karyawan);

		echo json_encode($obj);
	}
}
