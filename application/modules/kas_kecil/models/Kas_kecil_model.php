<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Kas_kecil_model extends CI_Model
{
    public function get_nasabah()
    {
        $query = $this->db->select("a.id,tipe_nasabah,a.kode_pos_id,a.mantri_id,a.running_number,CONCAT(a.tipe_nasabah, '.', b.nm_pos, '-', lpad(c.kode, 2, '0'), '.', lpad(a.running_number, 5, '0')) no_nasabah, a.nama_nasabah, a.ktp, a.kk, a.alamat_ktp, a.alamat_tinggal , a.AR, a.NT, b.nm_pos, a.sts_pending")
            ->from('m_nasabah a')
            ->join('m_kode_pos b', 'b.id = a.kode_pos_id')
            ->join('m_mantri c', 'c.id = a.mantri_id')
            ->where('a.active', 1)
            ->get();
        return $query->result_array();
    }

    public function add_nasabah(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->select('MAX(running_number + 1) as running_number')
        ->from('m_nasabah');
        $running_number = $this->db->get();
        $_POST['running_number'] = $running_number->row()->running_number;
        $this->db->insert('m_nasabah',$_POST);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambah data nasabah.'
            ];
        }
    }

    public function edit_nasabah(Type $var = null)
    {
        $this->db->trans_begin();
        $edit_nasabah = array(
            'nama_nasabah' => $_POST['nama_nasabah'],
            'kode_pos_id' => $_POST['kode_pos_id'],
            'mantri_id' => $_POST['mantri_id'],
            'ktp' => $_POST['ktp'],
            'kk' => $_POST['kk'],
            'alamat_ktp' => $_POST['alamat_ktp'],
            'alamat_tinggal' => $_POST['alamat_tinggal'],
        );
        $this->db->where('id', $_POST['id']);
        $this->db->update('m_nasabah', $edit_nasabah);
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil mengubah data nasabah.'
            ];
        }
    }

    public function delete_nasabah(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->set('active', 0);
        $this->db->where('id', $_POST['id']);
        $this->db->update('m_nasabah');
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menghapus data nasabah.'
            ];
        }
    }

    public function verif_nasabah(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->set('sts_pending', 1);
        $this->db->where('id', $_POST['id']);
        $this->db->update('m_nasabah');
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil memverifikasi nasabah.'
            ];
        }
    }
}
