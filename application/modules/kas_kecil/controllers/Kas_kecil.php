<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Kas_kecil extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kas_kecil_model');
		$this->load->model('Data_coa/Data_coa_model');
		$this->load->model('Auth/Ion_auth_model');
	}

	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view('sidebar', $data);
		$this->load->view('navbar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$data['parent_active'] = 3;
		$uri = &load_class('URI', 'core');
		$this->_render('Kas_kecil_view', $data);
	}

	public function get_nasabah(Type $var = null)
	{
		$nasabah = $this->Data_nasabah_model->get_nasabah();
		// var_dump($karyawan);die();
		// $data =  json_decode(json_encode($karyawan->result()), TRUE);
		$obj = array("data" => $nasabah);

		echo json_encode($obj);
	}

	public function add(Type $var = null)
	{
		$add = $this->Data_nasabah_model->add_nasabah();
		echo json_encode($add);
	}

	public function edit(Type $var = null)
	{
		$edit = $this->Data_nasabah_model->edit_nasabah();
		echo json_encode($edit);
	}

	public function delete(Type $var = null)
	{
		$delete = $this->Data_nasabah_model->delete_nasabah();
		echo json_encode($delete);
	}

	public function verif()
	{
		$verif = $this->Data_nasabah_model->verif_nasabah();
		echo json_encode($verif);
	}

	// public function get_selectCoa(Type $var = null)
	// {
	// 	$coa = $this->Data_coa_model->get_coa();
	// 	var_dump($coa);
	// 	die();
	// }
	public function get_selectCoa()
	{

		$coa = $this->Data_coa_model->get_coa();
		$list_coa = [];
		$kode_coa = [];
		foreach ($coa as $item) {
			array_push($kode_coa, $item->kode_coa);
			if ($item->kode_coa_1 == null) {
				$list_coa[$item->kode_coa]['kode_coa'] = $item->kode_coa;
				$list_coa[$item->kode_coa]['kelompok'] = $item->kelompok;
				$list_coa[$item->kode_coa]['description'] = $item->description;
				$list_coa[$item->kode_coa]['tipe'] = $item->tipe;
			} elseif ($item->kode_coa_2 == null) {
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa]['kode_coa'] = $item->kode_coa;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa]['kelompok'] = $item->kelompok;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa]['description'] = $item->description;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa]['tipe'] = $item->tipe;
			} elseif ($item->kode_coa_3 == null) {
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['id'] = $item->kode_coa;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['kelompok'] = $item->kelompok;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['description'] = $item->description;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa]['tipe'] = $item->tipe;
			} else {
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['id'] = $item->kode_coa;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['kelompok'] = $item->kelompok;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['description'] = $item->description;
				$list_coa[$item->kode_coa_1]['kode_coa1'][$item->kode_coa_2]['kode_coa2'][$item->kode_coa_3]['kode_coa3'][$item->kode_coa]['tipe'] = $item->tipe;
			}
		}
		$option = '<option>pilih coa</option>';



		if (isset($_GET['tipe']) == 'json') {
			$result = array('data' => $list_coa);
			echo json_encode($result);die();
		}

		foreach ($list_coa as $key => $coa1) {

			$option .= '<option value="' . $coa1['kode_coa'] . '">' . $coa1['description'] . '</option>';
			if (array_key_exists('kode_coa1', $coa1)) {

				foreach ($coa1['kode_coa1'] as $key => $coa2) {

					$option .= '<option value="' . $coa2['kode_coa'] . '">-' . $coa2['description'] . '</option>';
					if (array_key_exists('kode_coa2', $coa2)) {
						foreach ($coa2['kode_coa2'] as $key => $coa3) {
							$option .= '<option value="' . $coa3['kode_coa'] . '">--' . $coa3['description'] . '</option>';
							if (array_key_exists('kode_coa3', $coa3)) {
								foreach ($coa3['kode_coa3'] as $key => $coa4) {
									$option .= '<option value="' . $coa4['kode_coa'] . '">---' . $coa4['description'] . '</option>';
								}
							}
						}
					}
				}
			}
		}

		echo $option;
	}
}
