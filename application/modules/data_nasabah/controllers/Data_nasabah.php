<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Data_nasabah extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_nasabah_model');
		$this->load->model('Auth/Ion_auth_model');
	}

	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view('sidebar', $data);
		$this->load->view('navbar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$data['parent_active'] = 8;
		$data['child_active'] = 12;
		$uri = &load_class('URI', 'core');
		$data['mantri'] = $this->ion_auth_model->get_mantri();
		$data['kode_pos'] = $this->ion_auth_model->get_kode_pos();
		$this->_render('Data_nasabah_view', $data);
	}

	public function get_nasabah(Type $var = null)
	{
		$nasabah = $this->Data_nasabah_model->get_nasabah();
		// var_dump($karyawan);die();
		// $data =  json_decode(json_encode($karyawan->result()), TRUE);
		$obj = array("data" => $nasabah);

		echo json_encode($obj);
	}

	public function add(Type $var = null)
	{
		$add = $this->Data_nasabah_model->add_nasabah();
		echo json_encode($add);
	}

	public function edit(Type $var = null)
	{
		$edit = $this->Data_nasabah_model->edit_nasabah();
		echo json_encode($edit);
	}

	public function delete(Type $var = null)
	{
		$delete = $this->Data_nasabah_model->delete_nasabah();
		echo json_encode($delete);
	}

	public function verif()
	{
		$verif = $this->Data_nasabah_model->verif_nasabah();
		echo json_encode($verif);
	}
}
