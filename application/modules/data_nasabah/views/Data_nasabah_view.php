<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Data Nasabah</h6>

        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" data-toggle="modal" data-target="#modal-input" class="btn btn-sm btn-neutral">Tambah Nasabah</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">

  <div class='card'>
    <div class='card-header'>
      <h4 class='card-title'>Data Nasabah</h4>
      <div class="card-tools">
      </div>
    </div>

    <div class='card-body table-responsive p-3'>

      <table id='tabelNasabah' class='table table-bordered table-hover' style='width:100%'>
        <thead class="thead-light">
          <tr>
            <th>No Anggota</th>
            <th>Nama Anggota</th>
            <th>NIK</th>
            <th>NO KK</th>
            <th>ALAMAT KTP</th>
            <th>ALAMAT TEMPAT TINGGAL</th>
            <th>KODE POS</th>
            <th>AR</th>
            <th>NT</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-sm-12 col-md-5" id="dataTable_showing">

        </div>
        <div class="col-sm-12 col-md-7" id="dataTable_paginate">

        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>
<!-- input nasabah -->
<div class="modal fade" id="modal-input">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Nasabah</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="" method="post" id="form_input_nasabah" enctype="multipart/form-data" class="form-horizontal">
        <div class="modal-body ">
          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label for="">Kode Pos :</label>
                <select class="form-control" required data-width="100%" required placeholder="Pilih Mantri" name="kode_pos_id" id="kode_pos" data-toggle="select" title="Simple select" data-live-search="true" data-live-search-placeholder="Search ...">
                  <option selected value="">Pilih Kode Pos</option>
                  <?php foreach ($kode_pos as $kp) {
                    echo '<option value="' . $kp->id . '">' . $kp->nm_pos . '</option>';
                  } ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Mantri :</label>
                <select class="form-control" required placeholder="Pilih Mantri" name="mantri_id" id="mantri" data-toggle="select" title="Simple select" data-live-search="true" data-live-search-placeholder="Search ...">
                  <option selected value="">Pilih Matri</option>
                  <?php foreach ($mantri as $m) {
                    echo '<option value="' . $m->id . '">' . $m->nama . '</option>';
                  } ?>
                </select>
              </div>
            </div>

          </div>
          <div class="form-group">
            <label for="">Nama Nasabah :</label>
            <input type="text" class="form-control" name="nama_nasabah" id="input_nama_nasabah">
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">NIK :</label>
                <input type="number" class="form-control" name="ktp" id="input_ktp">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">No KK :</label>
                <input type="number" class="form-control" name="kk" id="input_kk">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="">Alamat KTP :</label>
            <textarea type="text" class="form-control" name="alamat_ktp" id="input_alamat_ktp">
            </textarea>
          </div>
          <div class="form-group">
            <label for="">Alamat Tempat Tinggal :</label>
            <textarea type="text" class="form-control" name="alamat_tinggal" id="input_alamat_tinggal">
            </textarea>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          <button type="Submit" value="Submit" id="btnSave" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- edit nasabah -->
<div class="modal fade" id="modal-edit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Nasabah</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" id="form_edit_nasabah" enctype="multipart/form-data" class="form-horizontal">
        <div class="modal-body ">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">No Nasabah :</label>
                <input type="text" readonly class="form-control" id="edit_no_nasabah">
                <input type="hidden" readonly name="id" id="edit_id">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Kode Pos :</label>
                <input type="text" class="form-control" name="kode_pos_id" id="edit_kode_pos">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Mantri :</label>
                <input type="text" class="form-control" name="mantri_id" id="edit_mantri">
              </div>
            </div>

          </div>
          <div class="form-group">
            <label for="">Nama Nasabah :</label>
            <input type="text" class="form-control" name="nama_nasabah" id="edit_nama_nasabah">
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">NIK :</label>
                <input type="number" class="form-control" name="ktp" id="edit_ktp">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">No KK :</label>
                <input type="number" class="form-control" name="kk" id="edit_kk">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="">Alamat KTP :</label>
            <textarea type="text" class="form-control" name="alamat_ktp" id="edit_alamat_ktp">
            </textarea>
          </div>
          <div class="form-group">
            <label for="">Alamat Tempat Tinggal :</label>
            <textarea type="text" class="form-control" name="alamat_tinggal" id="edit_alamat_tinggal">
            </textarea>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          <button type="Submit" value="Submit" id="btnEdit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- JS -->
<script>
  $(function() {
    $(document).ready(function() {
      list_nasabah();
      // $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
    });
  });

  function list_nasabah() {
    var siteTable = $('#tabelNasabah').DataTable({
      "ajax": '<?php echo base_url() ?>data_nasabah/get_nasabah',
      "order": [
        [0, "asc"]
      ],
      "language": {
        "paginate": {
          "next": '<i class="fas fa-angle-right"></i>',
          "previous": '<i class="fas fa-angle-left"></i>',
        }
      },
      initComplete: (settings, json) => {
        $("#dataTable_showing").append($(".dataTables_info"));
        $("#dataTable_paginate").append($(".dataTables_paginate"));
        $(".dataTables_paginate").addClass('float-right')
      },
      "columns": [{
          "data": 'no_nasabah',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'nama_nasabah',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'ktp',
          "className": 'align-middle text-nowrap',
          "width": '15%'
        },
        {
          "data": 'kk',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'alamat_ktp',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'alamat_tinggal',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'nm_pos',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'AR',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "data": 'NT',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },
        {
          "render": function(data, type, row, meta) {
            if (row.sts_pending == 0) {
              var a = '<button class="btn btn-outline-warning btn-sm btn-verif" title="Perlu Verifikasi">!Pending</button>';
            } else {
              var a = '<button class="btn btn-outline-success btn-sm btn-verif" title="Perlu Verifikasi">Approved</button>';
            }

            return a;
          }
        },
        {
          "defaultContent": '<button title="edit" class="btn btn-outline-primary btn-sm btn-edit mr-1">Edit</button><button title="hapus" class="btn btn-outline-danger btn-sm btn-delete">Hapus</button>',
          "className": 'align-middle text-nowrap text-center',
          "width": '1%'
        },

      ],
    });
    $('#tabelNasabah tbody').on('click', '.btn-verif', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      swal
        .fire({
          title: "Perhatian",
          html: "Verifikasi nasabah  <b>[" + datas.no_nasabah + "] " + datas.nama_nasabah + "</b>?",
          // type: "question",
          showCloseButton: true,
          showCancelButton: true,
          confirmButtonColor: "#00b894",
          confirmButtonText: "Verif",
          cancelButtonText: "Batal"
        })
        .then(function(result) {
          if (result.value) {
            $.ajax({
              url: <?php base_url() ?> 'data_nasabah/verif',
              method: "POST",
              data: {
                id: datas.id
              },
              dataType: "json",
              beforeSend: function() {
                swal.fire({
                  title: 'Please Wait..!',
                  text: 'Is working..',
                  onOpen: function() {
                    swal.showLoading()
                  }
                })
              },
              success: function(data) {
                swal.hideLoading();
                if (data.status == true) {
                  swal.fire({
                    icon: 'success',
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 2000
                  });
                  $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                  swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                }
              }
            })
          } else {}
        });
    });
    // delete nasabah
    $('#tabelNasabah tbody').on('click', '.btn-edit', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      $('#modal-edit').modal('show');
      $('#edit_id').val(datas.id);
      $('#edit_no_nasabah').val(datas.no_nasabah);
      $('#edit_nama_nasabah').val(datas.nama_nasabah);
      $('#edit_ktp').val(datas.ktp);
      $('#edit_kk').val(datas.kk);
      $('#edit_alamat_ktp').val(datas.alamat_ktp);
      $('#edit_alamat_tinggal').val(datas.alamat_tinggal);
      $('#edit_kode_pos').val(datas.kode_pos_id);
      $('#edit_mantri').val(datas.mantri_id);
    });
    // hapus nasabah
    $('#tabelNasabah tbody').on('click', '.btn-delete', function() {
      var datas = siteTable.row($(this).parents('tr')).data();
      console.log(datas);
      swal
        .fire({
          title: "Perhatian",
          html: "Hapus nasabah  <b>[" + datas.no_nasabah + "] " + datas.nama_nasabah + "</b>?",
          // type: "question",
          showCloseButton: true,
          showCancelButton: true,
          confirmButtonColor: "#e74c3c",
          confirmButtonText: "Hapus",
          cancelButtonText: "Tidak"
        })
        .then(function(result) {
          if (result.value) {
            $.ajax({
              url: <?php base_url() ?> 'data_nasabah/delete',
              method: "POST",
              data: {
                id: datas.id
              },
              dataType: "json",
              beforeSend: function() {
                swal.fire({
                  title: 'Please Wait..!',
                  text: 'Is working..',
                  onOpen: function() {
                    swal.showLoading()
                  }
                })
              },
              success: function(data) {
                swal.hideLoading();
                if (data.status == true) {
                  swal.fire({
                    icon: 'success',
                    title: data.msg,
                    showConfirmButton: false,
                    timer: 2000
                  });
                  $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                  swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                }
              }
            })
          } else {}
        });
    });
  }
  $('#form_input_nasabah').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'data_nasabah/add',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btnSave').attr('disabled', true);

      },
      success: function(data) {
        $('#btnSave').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          $('#modal-input').modal('hide');
        } else {
          swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
        };

      }
    });
  });
  $('#form_edit_nasabah').on('submit', function(event) {
    event.preventDefault();
    swal.fire({
      title: 'Please Wait..!',
      text: 'Is working..',
      onOpen: function() {
        swal.showLoading()
      }
    })
    $.ajax({
      url: <?php base_url() ?> 'data_nasabah/edit',
      type: "post",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      dataType: "JSON",
      beforeSend: function() {
        $('#btnEdit').attr('disabled', true);

      },
      success: function(data) {
        $('#btnEdit').attr('disabled', false);
        swal.hideLoading();
        if (data.status) {
          swal.fire({
            icon: 'success',
            title: data.msg,
            showConfirmButton: false,
            timer: 2000
          });
          $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
          $('#modal-edit').modal('hide');
        } else {
          swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
        };

      }
    });
  });
</script>