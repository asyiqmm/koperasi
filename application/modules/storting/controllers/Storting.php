<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Storting extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Storting_model');
	}

	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view('sidebar', $data);
		$this->load->view('navbar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		$data['nasabah'] = $this->Storting_model->get_nasabah();
		$this->_render('Storting_view',$data);
	}

	function tambah_aksi(){
		$tgl = $this->input->post('tgl');
		$nama = $this->input->post('nama');
		$no_trans = $this->input->post('no_trans');
		$kode_nasabah = $this->input->post('kode_nasabah');
		$nominal = $this->input->post('nominal');
		$status = $this->input->post('status');
 
		$data = array(
			'tgl' => date("Y-m-d", strtotime(str_replace('/', '-', $tgl))),
			'no_trans' => $no_trans,
			'kode_nasabah' => $kode_nasabah,
			'nominal' => $nominal,
			'status' => $status
			);
		$this->Storting_model->input_data($data,'t_storting');
		redirect('storting');
	}

	public function tampil_data(Type $var = null)
	{
		$storting = $this->Storting_model->tampil_data();
		// var_dump($karyawan);die();
		// $data =  json_decode(json_encode($karyawan->result()), TRUE);
		$obj = array("data" => $storting);

		echo json_encode($obj);
	}

	public function edit(Type $var = null)
	{
		$edit = $this->Storting_model->edit_storting();
		// var_dump($karyawan);die();s
		// $data =  json_decode(json_encode($karyawan->result()), TRUE);
		echo json_encode($edit);
	}

	public function delete(Type $var = null)
	{
		$delete = $this->Storting_model->delete_storting();
		echo json_encode($delete);
	}
	public function add(Type $var = null)
	{
		$add = $this->Storting_model->add_storting();
		echo json_encode($add);
	}
}
