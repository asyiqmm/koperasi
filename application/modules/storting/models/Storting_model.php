<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Storting_model extends CI_Model
{
	function tampil_data(){
		//return $this->db->get('t_droping');
		$this->db->select("*, 
		(select nama_nasabah from m_nasabah where kode_nasabah = a.kode_nasabah) as nama_nasabah,
		(select ktp from m_nasabah where kode_nasabah = a.kode_nasabah) as ktp, 
		DATE_FORMAT(tgl, '%d/%m/%Y') as formattgl,
		CASE 
			WHEN a.status = 'NT' THEN '0' 
			WHEN a.status = 'RNT' THEN a.nominal 
			WHEN a.status = 'PMT' THEN '0' else '0' end as kas_kecil,
		CASE 
			WHEN a.status = 'NT' THEN a.nominal 
			WHEN a.status = 'RNT' THEN '0' 
			WHEN a.status = 'PMT' THEN '0' 
			else '0' end as beban_tak_tertagih,
		CASE 
			WHEN a.status = 'RNT' THEN a.nominal * -1 
			else '0' end as lain_lain,
		CASE 
			WHEN a.status = 'NT' THEN a.nominal * -1 
			WHEN a.status = 'RNT' THEN a.nominal
			WHEN a.status = 'PMT' THEN a.nominal
			else '0' end as cadangan_tak_tertagih,
		CASE 
			WHEN a.status = 'NT' THEN '0' 
			WHEN a.status = 'RNT' THEN a.nominal * -1
			else a.nominal * -1 end as piutang_usaha,
		CASE 
			WHEN a.status = 'NT' THEN '0' 
			WHEN a.status = 'RNT' THEN a.nominal * 15.384 / 100
			else a.nominal * 15.384 / 100 end as ditangguhkan_jasa,
		CASE 
			WHEN a.status = 'NT' THEN '0' 
			WHEN a.status = 'RNT' THEN a.nominal * 23.077 / 100
			else a.nominal * 23.077 / 100 end as ditangguhkan_bunga,
		CASE 
			WHEN a.status = 'NT' THEN '0'
			WHEN a.status = 'RNT' THEN a.nominal * -1 * 15.384 / 100
			WHEN a.status = 'PMT' THEN '0'
			else a.nominal * -1 * 15.384 / 100 end as jasa,
		CASE 
			WHEN a.status = 'NT' THEN '0'
			WHEN a.status = 'RNT' THEN a.nominal * -1 * 23.077 / 100
			WHEN a.status = 'PMT' THEN '0'
			else a.nominal * -1 * 23.077 / 100 end as bunga
		")
		
    
		->from('t_storting as a')
		->where('a.active', 1)
		->order_by('a.tgl', 'DESC');
		$query = $this->db->get();
	
        return $query->result_array();
	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	function get_nasabah(){
		$this->db->select('*')
		->from('m_nasabah')
		->order_by('nama_nasabah', 'ASC');
		$query = $this->db->get();
        return $query->result_array();
	}

	public function edit_storting(Type $var = null)
    {
        $this->db->trans_begin();
        $edit_storting = array(
            'tgl' => date("Y-m-d", strtotime(str_replace('/', '-', $_POST['tgl']))),
            'nominal' => $_POST['nominal'],
            'id' => $_POST['id'],
        );
        $this->db->where('id', $_POST['id']);
        $this->db->update('t_storting', $edit_storting);
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil mengubah data storting.'
            ];
        }
    }

	public function delete_storting(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->set('active', 0);
        $this->db->where('id', $_POST['id']);
        $this->db->update('t_storting');
        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menghapus data storting.'
            ];
        }
    }

	public function add_storting(Type $var = null)
    {
		$kode_bulan = substr($_POST['tgl'], 3, 2);
        $this->db->trans_begin();
        $this->db->select('MAX(running_number + 1) as running_number')
		->where('kode_bulan', $kode_bulan)
        ->from('t_storting');
        $running_number = $this->db->get();
        $number = $running_number->row()->running_number;
		$kode_number = sprintf("%05s", $number);
		$no_trans = "STR.".$kode_bulan.".".$kode_number;
		$data = array(
			'tgl' => date("Y-m-d", strtotime(str_replace('/', '-', $_POST['tgl']))),
			'no_trans' => $_POST['no_trans'],
			'kode_nasabah' => $_POST['kode_nasabah'],
			'nominal' => $_POST['nominal'],
			'status' => $_POST['status'],
			'running_number' => $number,
			'kode_bulan' => $kode_bulan,
			'no_trans' => $no_trans
			);
        $this->db->insert('t_storting',$data);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambah data storting.'
            ];
        }
    }
}
